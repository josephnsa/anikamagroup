select s.codigo, s.descripcion, a.codigo, a.nombrecorto, '', p.codigo, p.descripcion, u.codigo, ifnull(ma.descripcion,''), ifnull(mo.descripcion,''),
			ifnull(co.descripcion,''), f.codigo, f.descripcion, null, '', null,
			sum(case when m.factor = 1 and d.cantidad < 0 then d.cantidad else (d.cantidad * m.factor) end * if(d.umedida_id = p.umedida_id, 1, if(d.umedida_id = p.ucompra_id, p.conversion, um.factor))),
			sum(case when m.factor = 1 and d.cantidad < 0 then (d.importemn * -1) else (d.importemn * m.factor) end),
			sum(case when m.factor = 1 and d.cantidad < 0 then (d.importeme * -1) else (d.importeme * m.factor) end),
			d.producto_id, a.sucursal_id, d.almacen_id, d.umedida_id, d.lote, d.vencimiento, d.serie, 0, '', null, p.familiapdto_id, p.marca_id, a.codsunat
		from ingresoalmacen_detalle d
		join ingresoalmacen i on d.parent_id = i.id
		join comun.producto p on d.producto_id = p.id
		left join comun.marca ma on p.marca_id = ma.id
		left join comun.modelo mo on p.modelo_id = mo.id
		left join comun.color co on p.color_id = co.id
		join comun.familiapdto f on p.familiapdto_id = f.id
		left join comun.umedida_conversion um on um.parent_id = p.umedida_id and um.umedida_id = d.umedida_id
		join comun.umedida u on p.umedida_id = u.id
		join comun.movimientotipo m on i.movimientotipo_id = m.id
		join almacen a on d.almacen_id = a.id
		join sucursal s on a.sucursal_id = s.id
		where i.estado <> 'ANULADO' and d.producto_id = '00021'
		group by d.producto_id, d.lote, u.id, a.sucursal_id, d.almacen_id