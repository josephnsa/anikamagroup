$("#frm_upd_permisos").validate({
    submitHandler: function (form) {
        var checkboxValores = "";
        $('input[name="order[]"]:checked').each(function () {
            var input = $(this).val();
            checkboxValores += $(this).val() + ",";
        });
        if (checkboxValores != "") {
            msgLoading("#preload_registrarupd");
            $("#btn_upd_permisos").prop("disabled", true);
            $.ajax({
                type: "POST",
                url: "usuarios/usuarios_permisosUpd/" + checkboxValores,
                data: $(form).serialize(),
                success: function (msg) {
                    if (msg.trim() !== '') {
                        mensaje("se ha actualizado los permisos", "e");
                        ocultar('#div_detalle');
                    } else {

                        mensaje("Error Inesperando actualizando los permisos!, vuelva a intentarlo", "r");
                    }
                },
                error: function (msg) {
                    mensaje("r", "Error Inesperando actualizando los permisos!, vuelva a intentarlo");

                }
            });
        } else {
            alert("Seleccione al menos un registro");
        }

    }
});

function ocultar(div) {
    $(div).hide("slow");
}

