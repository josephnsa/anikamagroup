$(function () {
    $("#Listar_Usuarios").bind('click', function () {
        get_div('usuarios/load_view_lista_usuarios', 'qry_listar_usuarios');
    });
});

//insertar producto utilizando jquery
$(document).ready(function () {
    $("#frm_registrar_usuarios").validate({
        rules: {
            txt_ins_nick: {
                required: true
            },
            txt_ins_clave: {
                required: true
            },
            cbo_personal: {
                required: true
            }
        },
        submitHandler: function (form) {
            $.ajax({
                type: "post",
                url: "usuarios/registrar_usuario",
                data: $(form).serialize(),
                success: function (msg) {
                    if (msg.trim() == 1) {
                        mensaje("Se ha registrado satisfactoriamente", "e");
                        limpiarForm(form);
                    } else {
                        mensaje("Horror !cuidado!", "r");
                    }
                },
                error: function (msg) {
                    mensaje("Error de proceso !Comunicarse con Sistemas!", "r");
                }
            });
        }
    });

})

function loadPermisos(id_usuario) {
    $("#div_detalle").show("slow");
    get_div('usuarios/loadPermisos/' + id_usuario, 'detalle_usuario');
}

function ocultar(div) {
    $(div).hide("slow");
}