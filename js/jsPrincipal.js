function initEvtDel(funcion){ 
    $(".ui-icon-trash").on("click",function(e){
        e.preventDefault();
        var fila = $(this).parents('tr');
        var fn =""+funcion+"("+$(fila).attr('id')+")"; 
        confirmar("Confirmar","¿Seguro que desea retirar el registro seleccionado?",fn);
    });
}

function initEvtUpd(url,title,alto,ancho,func_close){              
    $(".ui-icon-pencil").each(function(){
        $("#"+this.id).click(function(e){
            e.preventDefault();
            var fila =$(this).parents('tr');
            set_popup(url+$(fila).attr('id'),title,alto,ancho,'',func_close);                                                  
        })
    });
}

function msgbox(title,msg,tipo,funcion){
    var a = 1;
    var b = 100;
    var randomnumber = (a+Math.floor(Math.random()*b));
    $(function() {              
        if (tipo == 'confirmar') {
            var imagen = "<img src='images/ico_alerta.png' width='14' height='14'>"
                    
            //creamos la etiqueta div
            $( "body" ).append( "<div id='message"+randomnumber+"' title='"+title+"'>"+imagen+"<p>"+msg+"</p></div>");
                    
            $( "#message"+randomnumber ).dialog({
                modal: true,
                draggable: false,                
                buttons: {
                    Si: function() {
                        eval(funcion);
                        $( this ).dialog( "close" );
                    },
                    No: function() {
                        $( this ).dialog( "close" );
                    }
                },
                close: function() {
                    $("#message"+randomnumber).remove();
                }
            });
                    
        }else{
            if (tipo == 'error') {
                var imagen = "<img src='images/error.png' width='14' height='14'>"
            }
            if (tipo == 'alerta') {
                var imagen = "<img src='images/ok.png' width='14' height='14'>"
            }
                    
            //creamos la etiqueta div
            $( "body" ).append( "<div id='message"+randomnumber+"' title='"+title+"'>"+imagen+"<p>"+msg+"</p></div>");
                    
            $( "#message"+randomnumber ).dialog({
                modal: true,
                draggable: false,
                buttons: {
                    Ok: function() {
                        $( this ).dialog( "close" );
                    }
                },
                close: function() {
                    $("#message"+randomnumber).remove();
                }
            });
        }
                

    });
}

function initEvtOpc(clase_icono,funcion){    
    $(".ui-icon-"+clase_icono).each(function(){
        $("#"+this.id).click(function(e){
            e.preventDefault();
            var fila =$(this).parents('tr');
            eval(funcion);
        })
    });    
}

function initEvtOpcId(clase_icono,funcion,parametros){ 
    /*
*@parametros : solo aceptara datos con formato Objeto Json
*@parametros : devolvera un objeto json (por el momento)
*/   
    $(".ui-icon-"+clase_icono).each(function(){
        $("#"+this.id).click(function(e){
            e.preventDefault();
            var fila =$(this).parents('tr');
            var idObj = $(fila).attr('id'); 
            var param;
            if (parametros != undefined && parametros != '') {
                param = new Object(parametros);
                param.cx = idObj;               
            }else{
                param = idObj;
            }
            if (funcion != undefined && funcion != '') {
                var fn =""+funcion+"("+param+")";                 
                eval(fn);
            }else{
                console.log("La Funcion No Se Encuentra Definida");
            }            
        })
    });    
}
//Inicializar Evento Con Opcion Popup Con Envio Id Como Parametro
function initEvtOpcPopupId(clase_icono,url,title,alto,ancho,func_close){
    $(".ui-icon-"+clase_icono).each(function(){
        $("#"+this.id).click(function(e){
            e.preventDefault();
            var fila =$(this).parents('tr');
            set_popup(url+$(fila).attr('id'),title,alto,ancho,'',func_close); 
        })
    });    
}

function confirmar(title,msg,funcion){
    var a = 1;
    var b = 100;
    var randomnumber = (a+Math.floor(Math.random()*b));   
    $(".msgdlg" ).dialog( "destroy" );    
    $("body").append( "<div id='messageconfirma"+randomnumber+"' class='msgdlg' title='"+title+"'>"+msg+"</div>");
    if (funcion != undefined && funcion != '') {
        $("#messageconfirma"+randomnumber).dialog(
        {
            autoOpen: true,
            draggable: false,  
            height: 200,
            modal: true,
            position: 'center',
            resizable: false,            
            width: 300,
            buttons: {
                'Si': function() {
                    try{
                        eval(funcion);
                        $(this).dialog( 'close' );
                    }catch(er){
                        console.log(er);
                    }
                },
                'No': function() {
                    $(this).dialog( 'close' );
                }
            },
            close: function() {
                $(this).dialog('destroy').remove();
                $("#messageconfirma"+randomnumber).remove();
            }
        }
        );    
    }
}

function mensaje(msg,tip){
    if (tip=='e'){
        $.toast({
            heading: msg,
            text: 'Felicitaciones',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'success',
            hideAfter: 3500, 
            stack: 6
          });

    }
    else if (tip=='r') {
                $.toast({
            heading: msg,
            text: 'Error',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'error',
            hideAfter: 3500
            
          }); 
    } else if (tip=='a'){    
        $.toast({
            heading: msg,
            text: 'Alerta',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'warning',
            hideAfter: 3500
            
          });
    }    
}

function get_Busqueda(Url,div_name,accion,Parametros) {
    $(function(){        
        $('#'+Objcaja).keydown(function(event){
            if (event.keyCode == "13"){ 
                $.post(Url+Parametros, {        
                    accion:accion
                }, function(data){                     
                    $('#'+div_name).html(data);
                });
                
            }
        });    
    });        
}

function get_div(Url,div_name,parametro){
    var Rdn=(Math.floor(Math.random()*11));  
    $.post(Url, {
        Rdn:Rdn,        
        json:parametro    
    }, function(data){ 
        data = data.trim();  
        $('#'+div_name).html(data);       
    });
}

var y=0;
function set_popup(url,title,ancho,alto,parametro,func_close){
    var a = 1;
    var b = 100;
    var randomnumber = (a+Math.floor(Math.random()*b));
 
    //    $(".popedit" ).remove();
    //    $(".popedit" ).dialog( "destroy" ); 
    $("body").append("<div id='popupEdicion"+randomnumber+"' class='popedit' title='"+title+"'></div>");           
    $("#popupEdicion"+randomnumber).dialog({          
        autoOpen:false,      
        position: 1, //0=TOP        
        width:ancho,
        Height:'auto',
        minHeight:alto,
        resizable: false,
        modal:true ,
        open: function(event,ui){
            get_page(url,this.id,parametro);
        },
        close: function(){      
            eval(func_close);
            $(this).dialog('destroy').remove();                        
            
        }     
    });    
    $("#popupEdicion"+randomnumber).dialog('open');
    return $("#popupEdicion"+randomnumber);
}

function set_Date(cNotFecha, tipo){
    if(tipo=='NA'){//El datepicker 
        var c = new Date();
    
        var year_actual=c.getFullYear();
        var year=year_actual + 20;
        $("#"+cNotFecha).datepicker({
            changeYear: true,
            changeMonth: true,
            closeText: 'Cerrar',
            prevText: '&#x3c;Ant',
            nextText: 'Sig&#x3e;',
            currentText: 'Hoy',
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
            dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
            dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
            dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
            weekHeader: 'Sm',
            dateFormat: 'dd/mm/yy',
            yearRange: "1935:"+year+"",
            firstDay: 0,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: ''
        });
    }
    
    
    var f = new Date();
    
    var yearactual=f.getFullYear();
    $("#"+cNotFecha).datepicker({
        changeYear: true,
        changeMonth: true,
        closeText: 'Cerrar',
        prevText: '&#x3c;Ant',
        nextText: 'Sig&#x3e;',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
        dateFormat: 'dd/mm/yy',
        firstDay: 0,
        yearRange: "1935:"+yearactual+"",
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    });
    $("#"+cNotFecha).attr("readonly", true); //inhabilita las cajas de texto
}

function ValidarRangoFechas(cNotFechaInicial, cNotFechaFinal, dateFormat){
    var formatoFecha = dateFormat == undefined ? "dd/mm/yy" : dateFormat;
    var dates = $( "#"+cNotFechaInicial+", #"+cNotFechaFinal+"" ).datepicker({
        changeYear: true,
        changeMonth: true,
        closeText: 'Cerrar',
        prevText: '&#x3c;Ant',
        nextText: 'Sig&#x3e;',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
        weekHeader: 'Sm',
        dateFormat: formatoFecha,
        firstDay: 0,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: '',
        yearRange:'1970:2050',
        onSelect: function( selectedDate ) {
            var option = this.id == ""+cNotFechaInicial+"" ? "minDate" : "maxDate",
            instance = $( this ).data( "datepicker" ),
            date = $.datepicker.parseDate(
                instance.settings.dateFormat ||
                $.datepicker._defaults.dateFormat,
                selectedDate, instance.settings );
            dates.not( this ).datepicker( "option", option, date );
        }
    });
    $("#"+cNotFechaInicial).attr("readonly", true); 
    $("#"+cNotFechaFinal).attr("readonly", true); 
    var myDate = new Date();
    $("#"+cNotFechaInicial).datepicker('setDate',myDate);
}

/* LIMPIA UN FORMULARIO */
function limpiarForm(obj) {
    // enaviar asi: limpiarForm('#miForm');
    $(':input', $(obj)).each(function() {
        var type = this.type;
        var tag = this.tagName.toLowerCase();      
        if (type == 'text' || type == 'password' || tag == 'textarea' || type == 'hidden')
            this.value = '';       
        else if (type == 'checkbox' || type == 'radio')
            this.checked = false;
        else if (tag == 'select')
            this.selectedIndex = 0; //-1
    });
}

/* MASCARAS */
function mascaraCelular(obj){
    $(obj).mask("999-999-999");
}

/* MENSAJERIA */
function msgLoading(obj,msg){
    var newURL = window.location.protocol + "//" + window.location.host + "/guayano/img/gif-load.gif";
    if(msg == undefined){
        $(obj).html("<div id='msg_loading' style='color:#2D91D4;font-size:0.75em'><div class='gif_loading'></div><img src='"+newURL+"' />&nbsp;Cargando ... por favor espere.</div>");
    }else{
        $(obj).html("<div id='msg_loading' style='color:#2D91D4;font-size:0.75em'><div class='gif_loading'></div><img src='"+newURL+"' />&nbsp;"+" "+msg+"</div>");
    }
}

function msgAviso(obj,msg){ 
    if(msg == undefined){
        $(obj).html("<div id='msg_aviso' class='alert alert-info'><span class='ui-icon ui-icon-lightbulb' style='float: left; margin-right: .3em;'></span> <strong>¡Hey! ... </strong> No se encontraron registros.</div>");
    }else{
        $(obj).html("<div id='msg_aviso' class='alert alert-info'><span class='ui-icon ui-icon-lightbulb' style='float: left; margin-right: .3em;'></span> <strong>¡Hey! ... </strong> "+msg+"</div>");
    }
}

// mensaje de informacion con opcion de cerrarlo desde una X
function msgInfo(obj,msg){ 
    if(msg == undefined){
        $(obj).html('<div id="msg_information"><div class="alert alert-info alert-block"><a href="#" data-dismiss="alert" class="close">×</a><h4 class="alert-heading"><span class="ui-icon ui-icon-flag" style="float: left; margin-right: .3em;margin-left: 0"></span>Informaci&oacute;n !</h4>No se encontraron registros.</div></div>');
    }else{
        $(obj).html('<div id="msg_information"><div class="alert alert-info alert-block"><a href="#" data-dismiss="alert" class="close">×</a><h4 class="alert-heading"><span class="ui-icon ui-icon-flag" style="float: left; margin-right: .3em;margin-left: 0"></span>Informaci&oacute;n !</h4>'+msg+'</div></div>');
    }
}

function msgError(obj,msg){ 
    if(msg == undefined){
        $(obj).html("<div id='msg_error' class='alert alert-error'><span class='ui-icon ui-icon-alert' style='float: left; margin-right: .3em;'></span> <strong>¡Error! ... </strong> Ha ocurrido un error, vuelva a intentarlo.</div>");
    }else{
        $(obj).html("<div id='msg_error' class='alert alert-error'><span class='ui-icon ui-icon-alert' style='float: left; margin-right: .3em;'></span> <strong>¡Error! ... </strong> "+msg+"</div>");
    }
}

function msgAlerta(obj,msg){ 
    $(obj).html("<div id='msg_alert' class='alert alert-alert'><span class='ui-icon ui-icon-info' style='float: left; margin-right: .3em;'></span> <strong>¡Cuidado! ... </strong> "+msg+"</div>");
}

function msgLoadSave(obj,btn){ //preload al costado del boton
    $(btn).attr("disabled", "true");
    $(obj).html("<div id='msg_saving' style='display:inline;'><img src='../img/loading.gif'/></div>");
}

function msgLoadSaveRemove(btn){
    $("#msg_saving").remove()
    $(btn).removeAttr("disabled");
}

function initEvtClosePopup(objCerrar,ObjPopup){
    $(objCerrar).click(function(){
        $(ObjPopup).dialog("close");   
    })    
}

function ValidaCKEditor(IdTextArea){
    CKEDITOR.instances[IdTextArea].updateElement();
}


function NewCKEditor(IdTextArea){
    //CKEditor
    var instance = CKEDITOR.instances[IdTextArea];
    if (instance) {
        CKEDITOR.remove(CKEDITOR.instances[IdTextArea]);
    }
    $( '#'+IdTextArea ).ckeditor({
        toolbar : 'Full'//CKEditorBasic sirve para poner la barra de botones en basica
    });
//CKEditor
}

function ClearCKEditor(IdTextArea){
    var editor = CKEDITOR.instances[IdTextArea];
    if (editor) {
        editor.destroy(true);
    }
    //    CKEDITOR.replace(IdTextArea);
    NewCKEditor(IdTextArea)
}


function MostrarfechaActual(cNotFecha, Todos){
    if(Todos=='ALL'){
        $("#"+cNotFecha).datepicker({
            changeYear: true,
            changeMonth: true,
            closeText: 'Cerrar',
            prevText: '&#x3c;Ant',
            nextText: 'Sig&#x3e;',
            currentText: 'Hoy',
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
            dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
            dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
            dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
            weekHeader: 'Sm',
            dateFormat: 'dd/mm/yy',
            firstDay: 0,
            isRTL: false,
            yearRange: '1950:2012',
            showMonthAfterYear: false,
            yearSuffix: ''
        });
    }else if(Todos=='NA'){
        $("#"+cNotFecha).datepicker({
            changeYear: true,
            changeMonth: true,
            closeText: 'Cerrar',
            prevText: '&#x3c;Ant',
            nextText: 'Sig&#x3e;',
            currentText: 'Hoy',
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
            dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
            dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
            dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
            weekHeader: 'Sm',
            dateFormat: 'dd/mm/yy',
            firstDay: 0,
            isRTL: false,
     
            showMonthAfterYear: false,
            yearSuffix: '',
            minDate: new Date()
        });
    }
    var myDate = new Date();
//    $("#"+cNotFecha).datepicker('setDate',myDate);
}