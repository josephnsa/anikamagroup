$(function () {
    var ProductoId = $("#hid_upd_ProductoId").val();
    
    $("#btn_uploadMult_noticia").bind({
        click: function () {      
            $('#txt_upload_archivo_mult').uploadify('upload', '*');
        }
    });
    
     $("#btn_cancelMult_noticia").bind({
        click: function () {
            $('#txt_upload_archivo_mult').uploadify('cancel', '*');
        }
    });
    $("#txt_upload_archivo_mult").uploadify({
        'method': 'post',
        'swf': '../js/scripts_uploadify/uploadify.swf',
        'uploader': 'producto/productos_upload',
        'cancelImg': '../js/scripts_uploadify/images/cancel.png',
        'buttonText': 'Buscar Image',
        'multi': false,
        'fileTypeDesc': 'Archivos de texto...',
        'fileTypeExts': '*.gif; *.jpg; *.png',
        'auto': false,
        'onUploadStart': function (file) {
            $('#txt_upload_archivo_mult').uploadify('settings', 'formData', {
                'hid_upd_ProductoId': $("#hid_upd_ProductoId").val()
            });
        },
        'onUploadSuccess': function (file, data, response) {
            if (data.trim() == 1) {
                //alert('The file ' + file.name + ' was successfully uploaded with a response of ' + response + ':' + data);
                get_div('producto/popup_producto_unload/' + ProductoId, 'my_modal_producto');
                mensaje("Se ha actualizado el archivo multimedia correctamente!", "e");
                //updtNoticiasMult($("#hid_upload_nNotCodigo").val());                
            }
            else {
                mensaje("Error inesperado al subir archivo multimedia!", "r");
            }
        }
    });
});

function MultimediaDel(nMultCodigo) {
    var rdn = Math.random() * 11;
    var msg = confirm("¿Esta seguro de realizar la operacion?");
    if (msg) {
        $.post('producto/MultimediaDel/' + nMultCodigo, {
            rdn: rdn
        }, function (data) {
            if (data.trim() == 1) {
               updtNoticiasMult($("#hid_upd_ProductoId").val());
                mensaje("se ha eliminado el archivo correctamente!", "e");
            } else {
                mensaje("Error inesperado, no se ha podido eliminar la empresa!", "r");
            }
        });
//    return false;

    }
}
function updtNoticiasMult(nNotCodigo) {
    $.ajax({
        type: "POST",
        url: 'producto/load_listar_view_multimedia/' + nNotCodigo,
        cache: false,
        data: {
            nNotCodigo: nNotCodigo
        },
        success: function (data) {
            $("#c_qry_emp").html(data);
        },
        error: function (data) {
            alert(data);
        }
    }
    )
}



