$(function () {
    $("#txt_desc_producto").click(function () {
        document.getElementById("txt_codigo_producto").value = "";
    });
    $("#txt_codigo_producto").click(function () {
        document.getElementById("txt_desc_producto").value = "";
    });
    $("#Listar_Producto").bind('click', function () {
        get_div('producto/load_listar_view_productos', 'qry_listar_productos');
    });
});
$(document).ready(function () {
    $("#frm_registrar_producto").validate({
        rules: {
            txt_ins_pro_descripcion: {
                required: true
            },
            txt_ins_pro_presentacion: {
                required: true
            },
            txt_ins_pro_precio_carta: {
                required: true
            },
            cbo_sub_familia: {
                required: true
            },
            cbo_almacen: {
                required: true
            },
            cbo_ins_publicacion_delivery: {
                required: true
            }
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: "producto/productoIns",
                data: $(form).serialize(),
                success: function (msg) {
                    if (msg.trim() == 1) {
                        mensaje("Se ha registrado el producto correctamente", "e");
                        limpiarForm(form);
                    } else {
                        mensaje("Error inesperado registrando", "r");
                    }
                },
                error: function (msg) {
                    mensaje("Erros inesperado registrando nuestro producto", "r");
                }
            });
        }
    });
})
function editar_producto(productoid) {
    get_div('producto/popup_editar_producto/' + productoid, 'my_modal_producto');
}

function agregar_ingredientes(productoid) {
    get_div('producto/popup_agregar_ingredientes/' + productoid, 'my_modal_ingredientes');
}
function modal_buscar_insumos()
{
    get_div('producto/modal_buscar_insumos/', 'my_modal_insumo');
}
function buscar_producto()
{
    $("#myModal").html("");
    var valor = $("#txt_codigo_producto").val();
    var valor2 = $("#txt_desc_producto").val();
    var res = valor2.replace(/[/]/g, "_");
//    alert(res);
    if (valor === "") {
        msgLoading("#preload");
        get_div('producto/buscar_productoxcodigo/-0/' + res, 'myModal');
    } else {
        msgLoading("#preload");
        get_div('producto/buscar_productoxcodigo/' + valor + '/' + '-0', 'myModal');
    }


}
function eliminar_producto(productoid) {
    var rdn = Math.random() * 11;
    var msg = confirm("¿Esta seguro de realizar la operacion?");
    if (msg) {
        $.post('producto/productoDel/' + productoid, {
            rdn: rdn,
            productoid: productoid
        }, function (data) {
            if (data.trim() == 1) {
                mensaje("se ha eliminado el Cliente correctamente!", "e");
                //get_page('cliente/load_listar_view_cliente','qry_listar_cliente');
                get_div('cliente/load_listar_view_clientes', 'qry_listar_clientes');

            } else {
                mensaje("Error inesperado, no se ha podido eliminar el Cliente!", "r");
            }
        });
        return false;
    }
}
function agregar_insumos(insumo_id, descripcion, precio, descripcion_unidad_medida)
{
    $("#hid_insumo_id").val(insumo_id);
    $("#txt_desripcion_insumo").val(descripcion);
    $("#hid_precio").val(precio);
    $("#hid_unidad_medida").val(descripcion_unidad_medida);
    $('.bs-example-modal-lg').modal('hide');
    $("#txt_cantidad_insumo").focus();
}
function agregar_detalle_insumos()
{
    var agregar_insumo_id = $("#hid_insumo_id").val();
    var descripcion_insumo = $("#insumo_name").val();
    var var_precio = parseFloat($("#hid_precio").val());
    var var_cantidad = parseFloat($("#txt_cantidad_insumo").val());
    var var_importe = 0;
    var var_unidad_medida = $("#hid_unidad_medida").val();
    var var_unidad_receta = '';

    if (agregar_insumo_id !== '0') {
        $('#js-tabla tr').each(function () {
            var arrayproductoId = $(this).find('td').eq(0).html();

            if (arrayproductoId == agregar_insumo_id) {
                alert('Producto Agregado');
                eliminarTablaDetalleProducto(arrayproductoId);
            }
        });
        switch (var_unidad_medida) {
            case 'KILOGRAMO':
                var_cantidad = var_cantidad / 1000;
                var_unidad_receta = 'GRAMO';
                var_importe = (var_cantidad * var_precio).toFixed(2);
                break;
            case 'LITRO':
                var_cantidad = var_cantidad / 1000;
                var_unidad_receta = 'MILILITRO';
                var_importe = (var_cantidad * var_precio).toFixed(2);
                break;
            case 'BOTELLA':
                var_cantidad = var_cantidad / 1;
                var_unidad_receta = 'BOTELLA';
                var_importe = (var_cantidad * var_precio).toFixed(2);
                break;
            case 'ATADO':
                var_cantidad = var_cantidad / 1000;
                var_unidad_receta = 'GRAMO';
                var_importe = (var_cantidad * var_precio).toFixed(2);
                break;
            default:
                var_cantidad = var_cantidad / 1;
                var_unidad_receta = 'UNIDAD';
                var_importe = (var_cantidad * var_precio).toFixed(2);
        }
        $("#js-tabla tr:last").after("<tr id='tabla" + agregar_insumo_id + "'><td>" + agregar_insumo_id + "</td><td>" + descripcion_insumo + "</td><td>" + var_unidad_receta + "</td><td>" + var_cantidad + "</td><td>" + var_precio + "</td><td>" + var_importe + "</td><td><img id='img' style='text-align: center; cursor: pointer;' src='../img/eliminar.png' width='20' height='20' onclick='eliminar_detalle_insumo(" + agregar_insumo_id + ")'></td></tr>");
        $("#insumo_name").val("");
        $("#hid_precio").val("");
        $("#txt_cantidad_insumo").focus();
        $("#txt_cantidad_insumo").val("");
    }

}
function eliminar_detalle_insumo(insumo_id) {
    //$('#js-tabla tr:not(:first-child)').remove();
    var trs = $("#js-tabla tr").length;
    if (trs > 1)
    {
        // Eliminamos la ultima columna
        $("#js-tabla tr:last").remove();

    }
}

function sumar_sub_total() {
    var importetotal = 0;
    $('.amt').each(function () {
        importetotal += Number($(this).val());
    });

    $('#txttotal').val(importetotal);

}
function grabar_producto_insumo() {

    var descripcion_producto = $("#txt_ins_pro_descripcion").val();
    var presentacion = $("#txt_ins_pro_presentacion").val();
    var precio_carta = $("#txt_ins_pro_precio_carta").val();
    var sub_familia = $("#cbo_sub_familia").val();
    var almacen = $("#cbo_almacen").val();
    var publicacion_delivery = $("#cbo_ins_publicacion_delivery").val();
    var nombre_producto = $("#txt_ins_pro_nombre").val();

    if (descripcion_producto === '')
    {
        mensaje("Ingrese Descripcion al Producto!", "r");
    } else if (nombre_producto === '')
    {
        mensaje("Ingrese Nombre al Producto!", "r");
    } else if (presentacion === '')
    {
        mensaje("Ingrese Presentacion al Producto!", "r");
    } else if (precio_carta === '')
    {
        mensaje("Ingrese PrecioCarta al Producto!", "r");
    } else if (sub_familia === '0')
    {
        mensaje("Seleccione una SubFamilia!", "r");
    } else if (almacen === '0')
    {
        mensaje("Seleccione un Area de Produccion!", "r");
    } else if ($('#js-tabla').find('td').length > 0) {
        valores = new Array();
        $('#js-tabla tr').each(function () {
            var array_insumo_id = $(this).find('td').eq(0).html();
            var array_unidad_medida = $(this).find('td').eq(2).html();
            var array_cantidad = $(this).find('td').eq(3).html();
            var array_precio = $(this).find('td').eq(4).html();
            var array_sub_total = $(this).find('td').eq(5).html();

            valor = new Array(array_insumo_id, array_unidad_medida, array_cantidad, array_precio, array_sub_total);
            valores.push(valor);

        });
        var data = {
            descripcion_producto: descripcion_producto,
            presentacion: presentacion,
            precio_carta: precio_carta,
            sub_familia: sub_familia,
            almacen: almacen,
            publicacion_delivery: publicacion_delivery,
            nombre_producto: nombre_producto,
            valores: valores
        };
        asignar(data);
        $('#js-tabla tr:not(:first-child)').remove();
        $("#txt_ins_pro_descripcion").val("");
        $("#txt_ins_pro_nombre").val("");
        $("#txt_ins_pro_presentacion").val("");
        $("#txt_ins_pro_precio_carta").val("");
        $('#cbo_sub_familia > option[value="0"]').attr('selected', 'selected');
        $('#cbo_almacen > option[value="0"]').attr('selected', 'selected');
    } else
    {
        mensaje("Agrega un Insumo al Detalle!", "r");
    }
}
function asignar(data) {
    var result = confirm("¿Esta seguro de realizar la operacion?");
    if (result) {
        $.ajax({
            type: "POST",
            url: "producto/producto_insumo_ins",
            data: data,
            beforeSend: function () {
                msgLoading("#derivar_cargando");
            },
            success: function (msg) {
                $("#derivar_cargando").html("");
                if (msg.trim() === '1') {
                    mensaje("El producto se ha registrado correctamente!", "e");
                } else {
                    mensaje("El producto no se ha registrado correctamente!", "r");
                }
            },
            error: function (msg) {
                mensaje("El producto no se ha registrado correctamente!", "r");
            }
        });

    }
}
function productos_upload(ProductoId) {
    get_div('producto/popup_producto_unload/' + ProductoId, 'my_modal_producto');
}
function view_productos_upload(ProductoId) {
    get_div('producto/load_listar_view_multimedia/' + ProductoId, 'my_modal_producto');
}
