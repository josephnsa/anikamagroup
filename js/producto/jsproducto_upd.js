$(function () {
    $(document).ready(function () {
        $("#frm_upd_producto").validate({
            rules: {
                txt_upd_pro_nombre: {
                    required: true
                },
                txt_upd_pro_descripcion: {
                    required: true
                },
                txt_upd_pro_presentacion: {
                    required: true
                },
                txt_upd_pro_precio_carta: {
                    required: true
                },
                cbo_sub_familia_upd: {
                    required: true
                }
            },
            submitHandler: function (form) {
                $.ajax({
                    type: "post",
                    url: "producto/productoUpd",
                    data: $(form).serialize(),
                    success: function (msg) {
                        if (msg.trim() == 1) {
                            get_div('producto/load_listar_view_productos/', 'qry_listar_productos');
                            mensaje("Se ha actualizado el producto correctamente", "e");
                        } else {
                            mensaje("Error inesperado actualizando", "r");
                        }
                    },
                    error: function (msg) {
                        mensaje("Erros inesperado actualizando nuestro insumo", "r");
                    }
                });
            }
        });
        $("#btn_agregar_receta").click(function () {
            if ($("#hid_producto_cab_id").val() != 0) {
                
                var producto_id = $("#hid_producto_cab_id").val();
                var insumo_id = $("input#hid_insumo_update_id").val();
                var cantidad = $("#txt_cantidad_insumo_update").val();
                var unidad_medida = $("#hid_unidad_medida_update").val();
                var precio_unitario = $("#hid_precio_update").val();
                var sub_total = 0;
                var var_unidad_receta = '';
                
                switch (unidad_medida) {
                    case 'KILOGRAMO':
                        cantidad = cantidad / 1000;
                        var_unidad_receta = 'GRAMO';
                        sub_total=(cantidad * precio_unitario).toFixed(2);
                        break;
                    case 'LITRO':
                        cantidad = cantidad / 1000;
                        var_unidad_receta = 'MILILITRO';
                        sub_total=(cantidad * precio_unitario).toFixed(2);
                        break;
                    case 'BOTELLA':
                        cantidad = cantidad / 1;
                        var_unidad_receta = 'BOTELLA';
                        sub_total=(cantidad * precio_unitario).toFixed(2);
                        break;
                    case 'ATADO':
                        cantidad = cantidad / 1000;
                        var_unidad_receta = 'GRAMO';
                        sub_total=(cantidad * precio_unitario).toFixed(2);
                        break;
                    default:
                        cantidad = cantidad / 1;
                        var_unidad_receta = 'UNIDAD';
                        sub_total=(cantidad * precio_unitario).toFixed(2);
                }
                var data = {
                    producto_id: producto_id,
                    insumo_id: insumo_id,
                    cantidad: cantidad,
                    precio_unitario: precio_unitario,
                    unidad_medida: var_unidad_receta,
                    sub_total: sub_total
                };
                $.ajax({
                    type: "POST",
                    url: "producto/adicionar_insumo_receta",
                    data: data,
                    success: function (msg) {
                        if (msg.trim() == 1) {
                            mensaje("Se agrego el insumo satisfactoriamente", "e");
                            get_div('producto/popup_editar_producto/' + producto_id, 'my_modal_producto');
                        } else {
                            mensaje("Horror !cuidado!", "r");
                        }
                    },
                    error: function (msg) {
                        mensaje("La operacion no se ha registrado correctamente!", "r");
                    }
                });
            } else
            {
                mensaje("!Debe Ingresar un Producto !", "a");
                $("#producto").focus();
            }
        });
    });
});
function eliminar_insumo_receta(ProductoId, InsumoId) {
    var rdn = Math.random() * 11;
    var msg = confirm("¿Esta seguro de realizar la operacion?");
    if (msg) {
        $.post('producto/eliminar_receta_insumo/' + ProductoId + '/' + InsumoId, {
            rdn: rdn,
            ProductoId: ProductoId,
            InsumoId: InsumoId
        }, function (data) {
            if (data.trim() == 1) {
                mensaje("se ha eliminado correctamente!", "e");
                get_div('producto/popup_editar_producto/' + ProductoId, 'my_modal_producto');
            } else {
                mensaje("Error inesperado, no se ha podido eliminar el Cliente!", "r");
            }
        });
        return false;
    }
}
