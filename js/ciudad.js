$('#txtBuscarCiudad').keyup(function(){
	alert($('#txtBuscarCiudad').val());

	var text = $('#txtBuscarCiudad').val();
	var lt = $('#txtBuscarCiudad').val().length;
	// alert(lt);
	if (lt >= 11) {
		$('#tblListCiudades tbody').html('');
		$.post(baseurl+"implementacion/getCiudadByNombre",
		{texto : text},
		function(data){
			// alert(data);
			var obj = JSON.parse(data);
			var output = '';
			$.each(obj, function(i,item){
				output += 
				'<tr>' +
				'	<td>'+item.descripcion+'</td>' +
				'	<td>'+item.ruc+'</td>' +
				'	<td>'+item.id+'</td>' +
		        '  	<td><a class="btn btn-default"><i class="fa fa-eye"></i> &nbsp;Ver</a></td>' +
		        '</tr>';
			});
			$('#tblListCiudades tbody').append(output);

		});
	}else if(lt == 0){
		$('#tblListCiudades tbody').html('');
	}
	
});
