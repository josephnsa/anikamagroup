<?php

class Implementacion_model extends CI_Model {


 function __construct($id = null) {
     parent::__construct();
     //$this->bdcomun = $this->load->database('comun', TRUE);
     $this->load->database();
 }

 function getCodigo(){
   return $this->codigo;
 }

 function setCodigo($codigo){
   $this->codigo=$codigo;
 }

 function codigoCorre() {
     $query = $this->db->query("SELECT max(id)+1 as codigo FROM implementacion");
     return $query->result();
 }


function result($data){
	$sql = "CALL sp_agregarActividades(?)";
    $result = $this->db->query($sql,$data);
	return 1;
}

 function detalle($idImp) {

     $query = $this->db->query("SELECT codigo,descripcion,fecha_ini,fecha_ter,t.id,t.name as actividad,t.description,razon_social_ter
	FROM task t inner join implementacion i on t.project_id = i.id
	 inner join terceros ter on i.idTer = ter.idTer
	 where project_id = ? ; ",$idImp);
     return $query->result();
 }

   public  function impleXusuario() {

     $query = $this->db->query("SELECT id,codigo,descripcion,fecha_ini,fecha_ter,t.razon_social_ter,r.rol_nombre
     FROM usuario u inner join implementacion i on u.idUsu=i.idUsu inner join terceros t on t.idTer=i.idTer inner join rol r on u.rol_id = r.idRol
     where u.idUsu= ? and estado_imp = 'H' ;", $this->session->userdata('idUsu'));
     return $query->result();

 }


    public function actualizarImp($param){
        $campos = array(
			'codigo' => $param['codigo'],
			'descripcion' => $param['descripcion'],
			'fecha_ini' => $param['fecha_ini'],
			'fecha_ter' => $param['fecha_ter']
		);
		$this->db->where('id', $param['id']);
		$this->db->update('implementacion',$campos);

		return 1;
    }

	 public function eliminarImp($param){
        $campos = array(
			'estado_imp' => $param['estado_imp']
		);
		$this->db->where('id', $param['id']);
		$this->db->update('implementacion',$campos);

		return 1;
    }

  public function guardarImpl($param){

    $campos = array(
      'codigo' => $param['codigo'],
      'descripcion' => $param['descripcion'],
      'fecha_ini' =>date('d-m-y',strtotime(str_replace('/','-', $param['fecha_ini']))),
      'fecha_ter' => date('d-m-y',strtotime(str_replace('/','-', $param['fecha_ter']))),
      'idTer' => $param['idTer'],
      'idUsu' => $param['idUsu'],
	  'estado_imp' => $param['estado_imp']

    );
    $this->db->insert('implementacion',$campos);
      return $this->db->insert_id();

  }

}
