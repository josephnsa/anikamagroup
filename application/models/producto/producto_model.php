<?php

class Producto_model extends CI_Model {
    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->bdguayano = $this->load->database('guayano', TRUE);
    }

    function buscar_productoxcodigo($valor,$valor2) {
        $parametro = array($valor,$valor2);
        $query = $this->bdguayano->query("select s.codigo as idsucursal, s.descripcion as sucursal, a.codigo as idalmacen, a.nombrecorto as almacen, p.codigo as id_producto, p.descripcion as producto, u.codigo as umedida, ifnull(ma.descripcion,''), ifnull(mo.descripcion,''),
			ifnull(co.descripcion,''), f.codigo, f.descripcion, null, '', null,
			round(sum(case when m.factor = 1 and d.cantidad < 0 then d.cantidad else (d.cantidad * m.factor) end * if(d.umedida_id = p.umedida_id, 1, if(d.umedida_id = p.ucompra_id, p.conversion, um.factor)))) as stock,
			sum(case when m.factor = 1 and d.cantidad < 0 then (d.importemn * -1) else (d.importemn * m.factor) end),
			sum(case when m.factor = 1 and d.cantidad < 0 then (d.importeme * -1) else (d.importeme * m.factor) end),
			d.producto_id, a.sucursal_id, d.almacen_id, d.umedida_id, d.lote, d.vencimiento, d.serie, 0, '', null, p.familiapdto_id, p.marca_id, a.codsunat
		from ingresoalmacen_detalle d
		join ingresoalmacen i on d.parent_id = i.id
		join comun.producto p on d.producto_id = p.id
		left join comun.marca ma on p.marca_id = ma.id
		left join comun.modelo mo on p.modelo_id = mo.id
		left join comun.color co on p.color_id = co.id
		join comun.familiapdto f on p.familiapdto_id = f.id
		left join comun.umedida_conversion um on um.parent_id = p.umedida_id and um.umedida_id = d.umedida_id
		join comun.umedida u on p.umedida_id = u.id
		join comun.movimientotipo m on i.movimientotipo_id = m.id
		join almacen a on d.almacen_id = a.id
		join sucursal s on a.sucursal_id = s.id
		where i.estado <> 'ANULADO' and p.codigo=? or p.descripcion = ?
		group by p.codigo, d.lote, u.id, a.sucursal_id, d.almacen_id", $parametro);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return null;
        }
    }
    function buscar_producto_unico($valor,$valor2) {
        $parametro = array($valor,$valor2);
        $query = $this->bdguayano->query("select p.codigo as id_producto, p.descripcion as producto, f.descripcion,
			round(sum(case when m.factor = 1 and d.cantidad < 0 then d.cantidad else (d.cantidad * m.factor) end * if(d.umedida_id = p.umedida_id, 1, if(d.umedida_id = p.ucompra_id, p.conversion, um.factor)))) as stock
		from ingresoalmacen_detalle d
		join ingresoalmacen i on d.parent_id = i.id
		join comun.producto p on d.producto_id = p.id
		left join comun.marca ma on p.marca_id = ma.id
		left join comun.modelo mo on p.modelo_id = mo.id
		left join comun.color co on p.color_id = co.id
		join comun.familiapdto f on p.familiapdto_id = f.id
		left join comun.umedida_conversion um on um.parent_id = p.umedida_id and um.umedida_id = d.umedida_id
		join comun.umedida u on p.umedida_id = u.id
		join comun.movimientotipo m on i.movimientotipo_id = m.id
		join almacen a on d.almacen_id = a.id
		join sucursal s on a.sucursal_id = s.id
		where i.estado <> 'ANULADO' and p.codigo=? or p.descripcion=? 
		group by p.codigo", $parametro);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return null;
        }
	}
	/*function buscar_producto_unico($valor,$valor2) {
        $parametro = array($valor,$valor2);
        $query = $this->bdguayano->query("select p.codigo as id_producto, p.descripcion as producto, f.descripcion,
			round(sum(case when m.factor = 1 and d.cantidad < 0 then d.cantidad else (d.cantidad * m.factor) end * if(d.umedida_id = p.umedida_id, 1, if(d.umedida_id = p.ucompra_id, p.conversion, um.factor)))) as stock
		from ingresoalmacen_detalle d
		join ingresoalmacen i on d.parent_id = i.id
		join comun.producto p on d.producto_id = p.id
		left join comun.marca ma on p.marca_id = ma.id
		left join comun.modelo mo on p.modelo_id = mo.id
		left join comun.color co on p.color_id = co.id
		join comun.familiapdto f on p.familiapdto_id = f.id
		left join comun.umedida_conversion um on um.parent_id = p.umedida_id and um.umedida_id = d.umedida_id
		join comun.umedida u on p.umedida_id = u.id
		join comun.movimientotipo m on i.movimientotipo_id = m.id
		join almacen a on d.almacen_id = a.id
		join sucursal s on a.sucursal_id = s.id
		where i.estado <> 'ANULADO' and p.codigo like '%' or p.descripcion=? 
		group by p.codigo", $parametro);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return null;
        }
    }*/

}

?>