<?php

class welcome_model extends CI_Model {

    public function top_productos_ventas($Mes) {
        $query = $this->db->query("select round(sum(a.Cantidad),2)as Cantidad,DescripcionInsumo from docventa_detalle a
        inner join insumo b on b.InsumoId=a.InsumoId
        inner join docventa c on c.DocVentaId=a.DocVentaId
        where month(Fecha)='$Mes'
        and c.EstadoId=1 
        group by DescripcionInsumo
        order by sum(a.Cantidad) desc
        limit 10");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return null;
        }
    }

    public function top_clientes_ventas($Mes) {
        $query = $this->db->query("select count(DocVentaId)as CantidadVentas,sum(Total)as Total,RazonSocial from docventa a
        inner join cliente b on b.ClienteId=a.ClienteId
        where month(Fecha)='$Mes'
        and a.EstadoId=1 
        group by RazonSocial
        order by count(DocVentaId) desc
        limit 10");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return null;
        }
    }

    public function cumpleaños_clientes($FechaHoy) {
        $query = $this->db->query("Call sp_cumpleaños_clientes($FechaHoy)");
        $this->db->close();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }

    function mensaje_alerta() {
        $query = $this->db->query("SELECT D.ClienteId, A.DocVentaId,Fecha, FechaPago, DescTipoComprobante, "
                . "Numdoc, D.ClienteId,NrDocumento AS Documento,RazonSocial, round(A.Total,2)as Total, "
                . "round(IFNULL((SELECT SUM(Importe)FROM AmortizacionCliente AC WHERE AC.DocVentaId=A.DocVentaId),0),2)AS Abonado, "
                . "round(A.Total-IFNULL((SELECT SUM(Importe)FROM AmortizacionCliente AC WHERE AC.DocVentaId=A.DocVentaId),0),2) AS PorCobrar "
                . "FROM CuentasCobrar A INNER JOIN DocVenta D ON D.DocVentaId=A.DocVentaId "
                . "INNER JOIN TipoComprobante C ON C.TipoComprobanteId=D.TipoComprobanteId INNER JOIN Cliente B ON B.ClienteId=D.ClienteId"
                . " where FechaPago between curdate() and curdate() + interval 2 day");
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return null;
        }
    }

    function reporte_chart_canvas() {
        $query = $this->db->query("SELECT                                    
                                    CASE WHEN MONTH(Fecha) = 1 THEN 'enero'
                                    WHEN MONTH(Fecha) = 2 THEN 'febrero'
                                    WHEN MONTH(Fecha) = 3 THEN 'marzo'
                                    WHEN MONTH(Fecha) = 4 THEN 'abril'
                                    WHEN MONTH(Fecha) = 5 THEN 'mayo'
                                    WHEN MONTH(Fecha) = 6 THEN 'junio'
                                    WHEN MONTH(Fecha) = 7 THEN 'julio'
                                    WHEN MONTH(Fecha) = 8 THEN 'agosto'
                                    WHEN MONTH(Fecha) = 9 THEN 'septiembre'
                                    WHEN MONTH(Fecha) = 10 THEN 'octubre'
                                    WHEN MONTH(Fecha) = 11 THEN 'noviembre'
                                    WHEN MONTH(Fecha) = 12 THEN 'diciembre'
                                    END AS Mes,
                                    count(DocVentaId)AS Total from docventa
                                    GROUP BY MONTH(Fecha);");
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return NULL;
        }
    }

    public function nuevos_clientes() {
        $query = $this->db->query("SELECT count(ClienteId) as Clientes FROM cliente");
        if ($query->num_rows() > 0) {
            $row = $query->row();
            $Clientes = $row->Clientes;
            return $Clientes;
        } else {
            return null;
        }
    }

    public function total_compras() {
        $query = $this->db->query("SELECT sum(total) as Total_Compras FROM doccompra where estadoid=1 and tipocomprobanteid <> 07 and tipocomprobanteid <> 08");
        if ($query->num_rows() > 0) {
            $row = $query->row();
            $Total_Compras = $row->Total_Compras;
            return $Total_Compras;
        } else {
            return null;
        }
    }

    public function total_ventas() {
        $query = $this->db->query("SELECT sum(total) as Total_Ventas FROM docventa where estadoid=1 and tipocomprobanteid <> 07 and tipocomprobanteid <> 08");
        if ($query->num_rows() > 0) {
            $row = $query->row();
            $Total_Ventas = $row->Total_Ventas;
            return $Total_Ventas;
        } else {
            return null;
        }
    }

    public function total_productos() {
        $query = $this->db->query("SELECT count(ProductoId) as Productos FROM producto where EstadoId=1");
        if ($query->num_rows() > 0) {
            $row = $query->row();
            $Productos = $row->Productos;
            return $Productos;
        } else {
            return null;
        }
    }

    public function porcentaje_productos_ventas($Mes) {
        $query = $this->db->query("SELECT 
    dv.InsumoId,
    p.DescripcionInsumo,
    COUNT(dv.DocVentaId) AS sx,
    (SELECT 
            COUNT(dv.InsumoId) * 100 / COUNT(A.DocVentaId)
        FROM
            docventa_detalle AS A) AS porcentaje
FROM
    docventa_detalle dv
        INNER JOIN
    insumo p ON dv.InsumoId = p.InsumoId
        INNER JOIN
    docventa d ON d.DocVentaId = dv.DocVentaId
WHERE
    MONTH(Fecha) = '$Mes'
GROUP BY dv.InsumoId");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return null;
        }
    }

}

?>