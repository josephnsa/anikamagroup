<?php

class Usuario_model extends CI_Model {

    //DECLARACION DE VARIABLES
    private $nUsuId = '';
	private $usu_clave='';
    private $nombre = '';
	private $apellidos = '';
	private $estado='';
	private $ruta_foto='';
	private $usu_correo='';
	private $usu_fecha_creacion = '';
	private $idRol ='';
	private $celular ='';

    //CONSTRUCTOR DE LA CLASE
    function __construct($id = null) {
        parent::__construct();
        //$this->bdcomun = $this->load->database('comun', TRUE);
        $this->load->database();
    }

			///GET



    function getNUsuId() {
        return $this->nUsuId;
    }

	function getClave(){
		return $this->usu_clave ;
	}

    function getNombre() {
        return $this->nombre;
    }

    function getApellidos() {
        return $this->apellidos;
    }

	function getEstado(){
		return $this->estado ;
	}

    function getRutaFoto() {
        return $this->ruta_foto;
    }

	function getCorreo(){
		return $this->usu_correo;
	}

	function getFechaCreacion(){
		return $this->usu_fecha_creacion;
	}
	function getIdRol (){
		return $this->idRol;
	}

	function getCelular(){
		return $this->celular;
	}


	// SET

    function setNUsuId($nUsuId) {
        $this->nUsuId = $nUsuId;
    }

	function setClave($usu_clave){
		$this->usu_clave =$usu_clave;
	}

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setApellidos($apellidos) {
        $this->apellidos = $apellidos;
    }

	function setEstado($estado){
		$this->estado =$estado;
	}

	function setRutaFoto($ruta_foto){
		$this->ruta_foto =$ruta_foto;
	}

	function setCorreo($usu_correo){
		$this->usu_correo = $usu_correo;
	}

	function setFechaCreacion($usu_fecha_creacion){
		$this->usu_fecha_creacion = $usu_fecha_creacion;
	}

    function setIdRol($idRol) {
        $this->idRol = $idRol;
    }

	function setCelular($celular){
		$this->celular = $celular;
	}


    function autenticarz($data) {
        $query = $this->db->query("SELECT * FROM usuario u inner join rol r on u.rol_id = r.idRol WHERE usu_correo = ? and usu_clave = ? ;", $data);
        if ($query->num_rows() > 0) {
            $row = $query->row();
            $this->setNUsuId($row->idUsu);
			$this->setClave($row->usu_clave);
            $this->setNombre($row->usu_nombre);
            $this->setApellidos($row->usu_apellidos);
			$this->setEstado($row->usu_estado);
			$this->setRutaFoto($row->usu_ruta_foto);
            $this->setCorreo($row->usu_correo);
            $this->setFechaCreacion($row->usu_fecha_creacion);
			$this->setCelular($row->celular);
			$this->setIdRol($row->rol_nombre);
            return true;
        } else {
            return false;
        }
    }
     function autenticarResponsable($data) {
        $query = $this->db->query("SELECT * FROM usuarios_empresa WHERE usu_correo = ? and usu_clave = ? ;", $data);
        if ($query->num_rows() > 0) {
            $row = $query->row();
            $this->setNUsuId($row->idUsuarios);
            $this->setClave($row->usu_clave);
            $this->setNombre($row->usu_nombre);
            $this->setApellidos($row->usu_apellidos);
            $this->setEstado($row->usu_estado);
            $this->setRutaFoto($row->usu_ruta_foto);
            $this->setCorreo($row->usu_correo);
            $this->setFechaCreacion($row->usu_fecha_creacion);
			 $this->setCelular($row->celular);
			 $this->setIdRol($row->rol_id);
            return true;
        } else {
            return false;
        }
    }

	public function insertar($param){
		$campos = array(
		'usu_clave' => $param['usu_clave'],
		'usu_nombre' => $param['usu_nombre'],
		'usu_apellidos' => $param['usu_apellidos'],
		'usu_estado' => $param['usu_estado'],
		'usu_ruta_foto' => $param['usu_ruta_foto'],
		'usu_correo' => $param['usu_correo'],
		'usu_fecha_creacion' => $param['usu_fecha_creacion'],
		'rol_id' =>$param['rol_id'],
		'celular' => $param['celular']
		);
		$this->db->insert('usuario',$campos);
        
        return 1;

	}

  public function actualizarDatos($param){
    $campos = array(
    'usu_nombre' => $param['usu_nombre'],
    'usu_apellidos' => $param['usu_apellidos'],
    'usu_ruta_foto' => $param['usu_ruta_foto'],
    'usu_correo' => $param['usu_correo'],
    'celular' => $param['celular']
    );

    $this->db->where('idUsu',$this->session->userdata('idUsu'));
    $this->db->update('usuario',$campos);

    return 1;
  }
  public function   actualizarDatos2($param){
    $campos = array(
    'usu_nombre' => $param['usu_nombre'],
    'usu_apellidos' => $param['usu_apellidos'],
    'usu_correo' => $param['usu_correo'],
    'celular' => $param['celular']
    );

    $this->db->where('idUsu',$this->session->userdata('idUsu'));
    $this->db->update('usuario',$campos);

    return 1;
  }



}
