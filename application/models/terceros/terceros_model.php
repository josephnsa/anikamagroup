
<?php

class Terceros_model extends CI_Model {

//   //DECLARACION DE VARIABLES
// private $nUsuId = '';
// private $ruc_ter='';
// private $razon_social_te = '';
// private $tipo_ter = '';
// private $representante_ter='';
// private $email_ter='';
// private $estado_ter='';
//

function __construct($id = null) {
    parent::__construct();
    //$this->bdcomun = $this->load->database('comun', TRUE);
    $this->load->database();
}


public function insertar($param){
  $campos = array(
  'ruc_ter' => $param['ruc_ter'],
  'razon_social_ter' => $param['razon_social_ter'],
  'tipo_ter' => $param['tipo_ter'],
  'representante_ter' => $param['representante_ter'],
  'email_ter' => $param['email_ter'],
  'estado_ter' => $param['estado_ter']
  );
  $this->db->insert('terceros',$campos);
  return $this->db->insert_id();
}
}
