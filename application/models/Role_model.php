<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Clase Rol, provee el acceso a datos de la tabla Role.
 * @package com.ejemplo.application.model
 * @version 1.0 25/03/2014 16:00
 * @author Jose Rodriguez <josearodrigueze@gmail.com>
 */
class Role_model extends CI_Model {

	//private $table = 'usuarios_empresa ue';

	public function __construct() {
		parent::__construct();
	}

	/**
	 * Devuelve los Roles almacenados en BD.
	 * Si se le pasa $id devuelde el Rol asociado.
	 *
	 * @param 	Integer $id ID de Rol. Defualt 0.
	 * @return  Array
	 * @version 1.0 25/03/2014 16:00
	 * @author Jose Rodriguez <josearodrigueze@gmail.com>
	 */	
	public function get($idImple) {
        
		return (empty($id)) ? $this->All($idImple) : $this->One();
	}

	/**
	 * Devuelve los Roles almacenados en BD.
	 * @return Array
	 * @version 1.0 25/03/2014 16:00
	 * @author Jose Rodriguez <josearodrigueze@gmail.com>
	 */
	private function All($idImple) {
        $q = $this->db->query("SELECT idUsuarios,usu_nombre,usu_apellidos FROM usuarios_empresa ue inner JOIN implementacion i ON i.id = ue.idimple WHERE i.id = ? ",$idImple);
		return ($q->num_rows() > 0) ? $q->result_array() : FALSE;
	}

	/**
	 * Devuelve un Rol dado su ID.
	 * @param Integer $id
	 * @return Array
	 * @version 1.0 25/03/2014 16:00
	 * @author Jose Rodriguez <josearodrigueze@gmail.com>
	 */
	private function One() {
        
        $this->db->select('ue.idUsuarios,ue.usu_nombre,ue.usu_apellidos');
        $this->db->from('usuarios_empresa ue');
        $this->db->join('implementacion i','i.id = ue.idimple');
		$this->db->where('ue.idimple', $id);
//		$this->db->limit(3);
		$q = $this->db->get();
        
		return ($q->num_rows() > 0) ? $q->row_array() : FALSE;
	}
}

/* End of file role_model.php */
/* Location: ./application/models/projects/role_model.php */