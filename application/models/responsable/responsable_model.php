<?php

class Responsable_model extends CI_Model {


  function __construct($id = null) {
      parent::__construct();
      //$this->bdcomun = $this->load->database('comun', TRUE);
      $this->load->database();
  }


  public function registrar($param){

    $campos = array(
      'dni'                => $param['dni'],
      'usu_nombre'         => $param['usu_nombre'],
      'usu_apellidos'      => $param['usu_apellidos'],
      'usu_correo'         => $param['usu_correo'],
      'usu_clave'          => $param['usu_clave'],
      'celular'            => $param['celular'],
	  'usu_estado'         => $param['usu_estado'],
	  'usu_ruta_foto'      => $param['usu_ruta_foto'],
	  'usu_fecha_creacion' => $param['usu_fecha_creacion'],
	  'idimple'            => $param['idimple']

    );
    $this->db->insert('usuarios_empresa',$campos);
	 return $this->db->insert_id();
  }

//	public function detalle() {
//
//     $query = $this->db->query("SELECT ue.idUsuarios,concat(ue.usu_nombre,' ',ue.usu_apellidos) as nombreCompleto FROM usuarios_empresa ue inner join implementacion i on i.id= ue.idimple
//	 where ue.idUsuarios = ? ; ",$this->session->userdata('idUsu'));
//     return $query->result();
//
// }
    //metodo para cargar las actividades asignadas al responsable
 //    public function ActividadesXresponsable() {
 //
 //     $query = $this->db->query("SELECT ue.idimple,name,code,status,duration,start,progress,end,depends FROM assigs a inner join usuarios_empresa ue on a.roleId = ue.idUsuarios inner join task t on t.id = a.task_id
 //     where ue.idUsuarios= ? order by t.id asc; ",$this->session->userdata('idUsu'));
 //     return $query->result();
 //
 // }


 //metodo para cargar las actividades asignadas al responsable
  public function ActividadesXresponsable() {

   $query = $this->db->query("SELECT ue.idimple,name,code,status,duration,start,end,depends,progress FROM assigs a inner join usuarios_empresa ue on a.roleId = ue.idUsuarios inner join task t on t.id = a.task_id
   where ue.idUsuarios= ? order by t.id asc; ",$this->session->userdata('idUsu'));
   return $query->result();

    }

    public function ActividadePredecesora($id) {
  $query = $this->db->query("SELECT name,code,status,duration,start,end FROM assigs a
    inner join task t on t.id = a.task_id
    inner join implementacion i on t.project_id = i.id
    where i.id = ? order by t.id asc; ", $id );
 return $query->result();

}

	public function updateTaskResponsable($param){
        $campos = array(
			'responsable_id' => $param['responsable_id']
		);
		$this->db->where('id', $param['actividad_id']);
		$this->db->update('task',$campos);

		return 1;
    }

}
