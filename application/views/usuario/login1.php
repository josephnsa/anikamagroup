<!DOCTYPE html>
<html lang="es">
<head>
	<title>Login - Anikama</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="<?php echo URL_DISEÑO; ?>images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo URL_DISEÑO; ?>vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo URL_DISEÑO; ?>fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo URL_DISEÑO; ?>fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo URL_DISEÑO; ?>vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo URL_DISEÑO; ?>vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo URL_DISEÑO; ?>vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo URL_DISEÑO; ?>vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo URL_DISEÑO; ?>vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo URL_DISEÑO; ?>css/util.css">
	<link rel="stylesheet" type="text/css" href="<?php echo URL_DISEÑO; ?>css/main.css">



<!--===============================================================================================-->
</head>
<body>

	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-54">
				<form class="login100-form validate-form" action="<?php echo URL_MAIN; ?>usuario/autenticar" method="post">
					<span class="login100-form-title p-b-49">
						<center><img src="<?php echo URL_IMG; ?>anikama.png" style="padding-left: ;"; height="100px"; ></center>
						<center><img src="<?php echo URL_IMG; ?>anikamagroup.png" style="padding-left: ;"; height="100px"; ></center>
					</span>
					<p style="color:red;"><?php if(isset($mensaje)) echo $mensaje; ?></p>
					<?= form_error('txt_usuario');?>
					<div class="wrap-input100 validate-input m-b-23" data-validate = "Ingrese su Usuario">
						<span class="label-input100">Usuario</span>
						<input class="input100" type="email" name="txt_usuario" placeholder="example@anikama.com" value="<?= set_value('txt_usuario');?>">
						<span class="focus-input100" data-symbol="&#xf206;"></span>
					</div>

					<?= form_error('txt_password');?>
					<div class="wrap-input100 validate-input" data-validate="Ingrese su Contraseña">
						<span class="label-input100">Contraseña</span>
						<input class="input100" type="password" name="txt_password" placeholder="*****************" >
						<span class="focus-input100" data-symbol="&#xf190;"></span>

					</div>

					<div class="text-right p-t-8 p-b-31">
						<a href="#">
							¿Se te olvidó tu contraseña?
						</a>
					</div>

					<div class="container-login100-form-btn">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
							<button class="login100-form-btn">
								Iniciar Sesion
							</button>
						</div>
					</div>

					<div class="txt1 text-center p-t-54 p-b-20">
						<span>
							Redes Sociales
						</span>
					</div>

					<div class="flex-c-m">
						<a href="https://www.facebook.com/AnikamaGroup/" class="login100-social-item bg1">
							<i class="fa fa-facebook"></i>
						</a>

						<a href="https://twitter.com/anikamagroup?lang=es" class="login100-social-item bg2">
							<i class="fa fa-twitter"></i>
						</a>

						<a href="#" class="login100-social-item bg3">
							<i class="fa fa-google"></i>
						</a>
					</div>

					<div class="flex-col-c p-t-155">
						<span class="txt1 p-b-17">
							No tienes una Cuenta?
						</span>

						<a href="<?php echo URL_MAIN; ?>usuario/registro" class="txt2">
							Registrate
						</a>
						<i class="fa fa-long-arrow-right m-l-5"></i>
					</div>
				</form>
			</div>
		</div>
	</div>



	<script type="text/javascript" src="<?php echo URL_DISEÑO; ?>vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo URL_DISEÑO; ?>vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo URL_DISEÑO; ?>vendor/bootstrap/js/popper.js"></script>
	<script type="text/javascript" src="<?php echo URL_DISEÑO; ?>vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo URL_DISEÑO; ?>vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo URL_DISEÑO; ?>vendor/daterangepicker/moment.min.js"></script>
	<script type="text/javascript" src="<?php echo URL_DISEÑO; ?>vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo URL_DISEÑO; ?>vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->

<script type="text/javascript" src="<?php echo URL_DISEÑO; ?>js/main.js"></script>

</body>
</html>
