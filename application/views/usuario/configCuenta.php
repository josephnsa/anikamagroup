<?php $this->load->view('dashboard/header'); ?>

<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<link href="<?php echo URL_CSS ; ?>configCuenta.css" rel="stylesheet">
<script type="text/javascript">
			function readURL(input){
				if(input.files && input.files[0]){
					var reader = new FileReader();
					reader.onload = function (e) {
						$("#Image1")
						.attr('src',e.target.result)
						.width(200)
						.height(190);
					};
					reader.readAsDataURL(input.files[0]);

				}
			}
	</script>
<div class="container">
    <div class="view-account">
        <section class="module">
            <div class="module-inner">

                <div class="content-panel">

                    <form class="form-horizontal" action="<?php echo URL_MAIN; ?>usuario/actualizarDatos" method="post" enctype="multipart/form-data">
                        <fieldset class="fieldset">
                            <h3 class="fieldset-title">Informacion Personal</h3>
                            <style type="text/css">
                      				input[type=file]{
                      					color:transparent;
                      				}
                      			</style>

                            <div class="form-group avatar">
                                <div class="form-inline col-md-3 col-sm-9 col-xs-12">
                                  <center>
                                    <img id="Image1" style="height: 300px;width: 300px;"  class="img-circle img-rounded img-responsive" src="<?php echo URL_MAIN; ?>uploads/imagenes/<?php echo $this->session->userdata('usu_ruta_foto');?>" alt="">

                                  <button class="" id="files" onclick="document.getElementById('file').click(); return false">Editar Perfil</button>
                                  <input  type="file" name="fileImagen" onchange="readURL(this);" style="visibility: hidden;" id="file" accept="image/*">
                                </center>
                                </div>
                            </div>




                            <div class="form-group">
                                <label class="col-md-2 col-sm-3 col-xs-12 control-label">Nombre</label>
                                <div class="col-md-10 col-sm-9 col-xs-12">
                                    <input name="txt_nombre" type="text" class="form-control" value="<?php echo $this->session->userdata('usu_nombre'); ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 col-sm-3 col-xs-12 control-label">Apellidos</label>
                                <div class="col-md-10 col-sm-9 col-xs-12">
                                    <input name="txt_apellidos" type="text" class="form-control" value="<?php echo $this->session->userdata('usu_apellidos'); ?>">
                                </div>
                            </div>

                            <div class="form-row">

                            <div class="form-group">
                                <label class="col-md-10 col-sm-3 col-xs-12 control-label">Contraseña</label>
                                <div class="col-md-10 col-sm-9 col-xs-12">
                                    <input  id="idContra" type="password" class="form-control" value="<?php echo $this->session->userdata('usu_clave'); ?>" >
                                </div>

                            </div>

                            <div class="form-group">
                                <label class="col-md-10 col-sm-3 col-xs-12 control-label">Nueva Contraseña</label>
                                <div class="col-md-10 col-sm-9 col-xs-12">
                                    <input id="checkNueva" type="password" class="form-control" disabled>
                                </div>

                            </div>

                          </div>
                          <div class="form-group">
                            <input id="checkContra" name="cambiarContra" type="checkbox" class="filled-in chk-col-light-blue" onclick="condicion ()">
                            <label for="checkbox-signup" style="color:brown;"> Cambiar Contraseña </label>

                          </div>


                        </fieldset>
                        <fieldset class="fieldset">
                            <h3 class="fieldset-title">Informacion de Contacto</h3>
                            <div class="form-group">
                                <label class="col-md-2  col-sm-3 col-xs-12 control-label">Email</label>
                                <div class="col-md-10 col-sm-9 col-xs-12" >
                                    <input name="txt_email" type="email" class="form-control" value="<?php echo $this->session->userdata('usu_correo'); ?>">

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2  col-sm-3 col-xs-12 control-label">Celular</label>
                                <div class="col-md-10 col-sm-9 col-xs-12" >
                                    <input name="txt_celular" type="text" class="form-control" value="<?php echo $this->session->userdata('celular'); ?>">

                                </div>
                            </div>

                        </fieldset>
                        <hr>
                        <div class="form-group">
                            <div class="col-md-10 col-sm-9 col-xs-12 col-md-push-2 col-sm-push-3 col-xs-push-0">
                                <input class="btn btn-primary" type="submit" value="Actualizar Datos">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
                <script>
                  function condicion(){

                      document.getElementById("checkNueva").disabled = false;
                      document.getElementById("idContra").value="";
                    }



        </script>
    </div>
</div>

<?php $this->load->view('dashboard/footer'); ?>
