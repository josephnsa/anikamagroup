<!DOCTYPE html>
<html lang="es">
<head>
	<title>Login - Anikama</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="<?php echo URL_DISEÑO; ?>images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo URL_DISEÑO; ?>vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo URL_DISEÑO; ?>fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo URL_DISEÑO; ?>fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo URL_DISEÑO; ?>vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo URL_DISEÑO; ?>vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo URL_DISEÑO; ?>vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo URL_DISEÑO; ?>vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo URL_DISEÑO; ?>vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo URL_DISEÑO; ?>css/util.css">
	<link rel="stylesheet" type="text/css" href="<?php echo URL_DISEÑO; ?>css/main.css">
<!--===============================================================================================-->
     <script src="<?php echo URL_ASS; ?>node_modules/jquery/jquery.min.js"></script>

    <script src = " https://unpkg.com/sweetalert/dist/sweetalert.min.js " > </script> 
</head>
<body>

	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-54">

				<form class="login100-form validate-form" method="post">
					<span class="login100-form-title p-b-49">
						<center><img src="<?php echo URL_IMG; ?>anikama.png" style="padding-left: ;"; height="100px"; ></center>
						<center><img src="<?php echo URL_IMG; ?>anikamagroup.png" style="padding-left: ;"; height="100px"; ></center>
					</span>

					<div class="wrap-input100 validate-input m-b-23" data-validate = "Ingrese Usuario">
						<span class="label-input100">Nombre</span>
						<input class="input100" type="text" name="txt_nombre" placeholder="Nombre" id="txt_nombre">
						<span class="focus-input100" data-symbol="&#xf206;"></span>
					</div>
					<div class="wrap-input100 validate-input m-b-23" data-validate = "Ingrese sus Apellidos">
						<span class="label-input100">Apellidos</span>
						<input class="input100" type="text" name="txt_apellidos" placeholder="Apellidos" id="txt_apellidos">
						<span class="focus-input100" data-symbol="&#xf206;"></span>
					</div>
					<div class="wrap-input100 validate-input m-b-23" data-validate = "Ingrese su Correo">
						<span class="label-input100">Correo</span>
						<input class="input100" type="text" name="txt_email" placeholder="example@gmail.com" id="txt_email">
						<span class="focus-input100" data-symbol="&#x2709;"></span>
					</div>
					<div class="wrap-input100 validate-input m-b-23" data-validate = "Ingrese su Celular">
						<span class="label-input100">Celular</span>
						<input class="input100" type="text" id="txt_celular" name="txt_celular" placeholder="Celular">
						<span class="focus-input100" data-symbol="&#x2706;"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Ingrese su Contraseña">
						<span class="label-input100">Contraseña</span>
						<input class="input100" type="password" id="txt_password" name="txt_password" placeholder="Contraseña">
						<span class="focus-input100" data-symbol="&#xf190;"></span>
					</div>
					
					<div class="text-right p-t-8 p-b-31">
						 <input hidden title="Debe Aceptar Terminos y Condiciones" id="checkbox-signup" type="checkbox" class="filled-in chk-col-light-blue" required>
					</div>


					<div class="container-login100-form-btn">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
							<button class="login100-form-btn" id="btnRegistroUsuario">
								Registrarse
							</button>
						</div>
					</div>

					<div class="txt1 text-center p-t-54 p-b-20">
						<span>
							Redes Sociales
						</span>
					</div>

					<div class="flex-c-m">
						<a href="#" class="login100-social-item bg1">
							<i class="fa fa-facebook"></i>
						</a>

						<a href="#" class="login100-social-item bg2">
							<i class="fa fa-twitter"></i>
						</a>

						<a href="#" class="login100-social-item bg3">
							<i class="fa fa-google"></i>
						</a>
					</div>

					<div class="flex-col-c p-t-155">
						<span class="txt1 p-b-17">
							ya tienes una cuenta?
						</span>

						<a href="<?php echo URL_MAIN; ?>usuario/cargarLogin" class="txt2">
							Inicia Sesion
						</a>
						
					</div>
				</form>
			</div>
		</div>
	</div>

 <script type="text/javascript">
    
    $(document).ready(function () { 
         var baseurl = "<?php echo base_url();?>";
    $('#btnRegistroUsuario').click(function(){
        var nombre = $('#txt_nombre').val();
	   var apellidos = $('#txt_apellidos').val();
	   var correo = $('#txt_email').val();
	   var celular = $('#txt_celular').val();
	   var contraseña = $('#txt_password').val();
        
        $.post(baseurl+"usuario/registrar",	
	{
		txt_nombre:nombre,
		txt_apellidos:apellidos,
		txt_email:correo,
		txt_celular:celular,
		txt_password:contraseña
	},			
	function(data){
		if (data == 1) {        
            swal({
              title: "Registro Completado!",
              text: "Bienvenido! "+nombre +" "+ apellidos +" Gracias por registrarte",
              icon: "success",
              button: "Listo!",
            }).then((value) => {
                 location.href = baseurl+'usuario/login'; 
            });
		}               
	});    
});
        
});
</script>

	<script type="text/javascript" src="<?php echo URL_DISEÑO; ?>vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo URL_DISEÑO; ?>vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo URL_DISEÑO; ?>vendor/bootstrap/js/popper.js"></script>
	<script type="text/javascript" src="<?php echo URL_DISEÑO; ?>vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo URL_DISEÑO; ?>vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo URL_DISEÑO; ?>vendor/daterangepicker/moment.min.js"></script>
	<script type="text/javascript" src="<?php echo URL_DISEÑO; ?>vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo URL_DISEÑO; ?>vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	

</body>
</html>
