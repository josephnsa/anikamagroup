<!DOCTYPE html>
<html lang="es">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Favicon icon -->    
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo URL_IMG; ?>favicon.png">
        <title>Login - Anikama</title>
        <!-- Bootstrap Core CSS -->
        <!--<link href="../assets/node_modules/bootstrap/css/bootstrap.min.css" rel="stylesheet">-->
        <link href="<?php echo URL_ASS; ?>node_modules/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- page css -->
        <!--<link href="css/pages/login-register-lock.css" rel="stylesheet">-->
        <link href="<?php echo URL_CSS; ?>pages/login-register-lock.css" rel="stylesheet">
        <!-- Custom CSS -->
        <!--<link href="css/style.css" rel="stylesheet">-->
        <link href="<?php echo URL_CSS; ?>style.css" rel="stylesheet">
        <!-- You can change the theme colors from here -->
        <!--<link href="css/colors/default.css" id="theme" rel="stylesheet">-->
        <link href="<?php echo URL_CSS; ?>colors/default.css" id="theme" rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    </head>

    <body class="card-no-border">
        <!-- ============================================================== -->
        <!-- Preloader - style you can find in spinners.css -->
        <!-- ============================================================== -->
        <div class="preloader">
            <div class="loader">
                <div class="loader__figure"></div>
                <p class="loader__label">Admin Wrap</p>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Main wrapper - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <section id="wrapper">
            <div class="login-register" style="background-image:url(<?php echo URL_ASS; ?>/images/background/login-register.jpg);padding-top: 20px; padding-bottom: 20px;">
                <div class="login-box card">
                    <div class="card-body">
                        <form class="form p-t-20" id="loginform" action="<?php echo URL_MAIN; ?>usuario/autenticar" method="post">    
							<h3 class="box-title m-b-20"><center><b>ANIKAMA GROUP<b></b></b></center></h3>
                            <center><img src="<?php echo URL_IMG; ?>anikama.png" style="padding-left: ;"; height="100px"; ></center>
                            <div class="form-group">
                                <label for="exampleInputuname">USUARIO</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1"><i class="ti-user"></i></span>
                                    </div>
                                    <input type="text" name="txt_usuario" id="txt_email" class="form-control" placeholder="Usuario" aria-label="Username" required="" aria-describedby="basic-addon1">
                                </div>
                            </div>
                            <div class="form-group">
                                        <label for="pwd1">CONTRASEÑA</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon3"><i class="ti-lock"></i></span>
                                            </div>
                                            <input type="password" name="txt_password" id="txt_password" class="form-control" placeholder="Password" aria-label="Password" aria-describedby="basic-addon3">
                                        </div>
                            </div>                           
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <div class="checkbox checkbox-info pull-left p-t-0">
                                        <input id="checkbox-signup" type="checkbox" class="filled-in chk-col-light-blue">
                                        <label for="checkbox-signup"> Recordar Usuario </label>
                                    </div> <a href="javascript:void(0)" id="to-recover" class="text-dark pull-right"><i class="fa fa-lock m-r-5"></i> Olvido su contraseña?</a> </div>
                            </div>
                            <div class="form-group text-center">
                                <div class="col-xs-12 p-b-20">
                                    <button class="btn btn-block btn-lg btn-info btn-rounded" type="submit">Inicio Sesión</button>
									
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 m-t-10 text-center">
                                    <div class="social">
                                        <a href="javascript:void(0)" class="btn  btn-facebook" data-toggle="tooltip" title="Login with Facebook"> <i aria-hidden="true" class="fa fa-facebook"></i> </a>
                                        <a href="javascript:void(0)" class="btn btn-googleplus" data-toggle="tooltip" title="Login with Google"> <i aria-hidden="true" class="fa fa-google-plus"></i> </a>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group m-b-0">
                                <div class="col-sm-12 text-center">
                                   ¿No tienes una cuenta? <a href="<?php echo URL_MAIN; ?>usuario/cargarLogin" class="text-info m-l-5"><b>Registrate</b></a>
                                </div>
                            </div>
							
                        </form>			
                    </div>
                </div>
            </div>
        </section>

        <!-- ============================================================== -->
        <!-- End Wrapper -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- All Jquery -->
        <!-- ============================================================== -->
        <!--<script src="../assets/node_modules/jquery/jquery.min.js"></script>-->
        <script src="<?php echo URL_ASS; ?>node_modules/jquery/jquery.min.js"></script>
        <!-- Bootstrap tether Core JavaScript -->
        <script src="<?php echo URL_ASS; ?>node_modules/bootstrap/js/popper.min.js"></script>
        <script src="<?php echo URL_ASS; ?>node_modules/bootstrap/js/bootstrap.min.js"></script>

<!--<script src="../assets/node_modules/bootstrap/js/popper.min.js"></script>
<script src="../assets/node_modules/bootstrap/js/bootstrap.min.js"></script>-->
        <!--Custom JavaScript -->
        <script type="text/javascript">
            $(function () {
                $(".preloader").fadeOut();
            });
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            });
            // ============================================================== 
            // Login and Recover Password 
            // ============================================================== 
            $('#to-recover').on("click", function () {
                $("#loginform").slideUp();
                $("#recoverform").fadeIn();
            });
        </script>
        <script type="text/javascript">
            $('#txt_email').keyup(function(){
             var datos = new String($('#txt_email').val());
                datos = datos.toUpperCase(datos);
                $('#txt_email').html(datos);
                $('#txt_email').val(datos);
                });
        </script>

    </body>

</html>