<?php $this->load->view('dashboard/header'); ?>
<script type="text/javascript" src='<?php echo URL_JS; ?>producto/jsproducto.js'></script>
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!--    <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">Tabs</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item active">Tabs</li>
                </ol>
            </div>
            <div class="col-md-7 align-self-center text-right d-none d-md-block">
                <button type="button" class="btn btn-info"><i class="fa fa-plus-circle"></i> Create New</button>
            </div>
            <div class="">
                <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
            </div>
        </div>-->
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">MANTENIMIENTO PRODUCTO</h4><br>
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">LISTAR PRODUCTOS</span></a> </li>
                        <!--<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#listar_productos" role="tab" id="Listar_Producto"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">LISTAR</span></a> </li>-->                        
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content tabcontent-border">
                        <div class="tab-pane active" id="home" role="tabpanel">
                            <div class="p-20">
                                <?php $this->load->view('producto/ins_view'); ?>
                            </div>
                        </div>
<!--                        <div class="tab-pane  p-20" id="listar_productos" role="tabpanel">
                            <div id="qry_listar_productos"></div>
                        </div>                        -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- row -->
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->

<?php $this->load->view('dashboard/footer'); ?>