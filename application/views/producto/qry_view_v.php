 <meta charset="utf-8">
<script>
    $(function () {
        $('#myTable').DataTable();

        var table = $('#example').DataTable({
            "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
            "order": [
                [2, 'asc']
            ],
            "displayLength": 25,
            "drawCallback": function (settings) {
                var api = this.api();
                var rows = api.rows({
                    page: 'current'
                }).nodes();
                var last = null;
                api.column(2, {
                    page: 'current'
                }).data().each(function (group, i) {
                    if (last !== group) {
                        $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                        last = group;
                    }
                });
            }
        });
        // Order by the grouping
        $('#example tbody').on('click', 'tr.group', function () {
            var currentOrder = table.order()[0];
            if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                table.order([2, 'desc']).draw();
            } else {
                table.order([2, 'asc']).draw();
            }
        });

    });
    $('#example23').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });

</script>
<script>
    $(document).ready(function () {
        $("#preload").html("");
    })
</script>
<!-- page css -->
<link href="<?php echo URL_CSS; ?>pages/card-page.css" rel="stylesheet">



<?php if ($this->session->userdata('estipo') == '1') { ?>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <div class="table-responsive m-t-40">
                        <?php if ($vproductos > 0) { ?>
                            <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Codigo</th>
                                        <th>Producto</th>                                
                                        <th>Sucursal</th>
                                        <th>Almacen</th>
                                        <th>Unidad Medida</th>
                                        <th>Descripcion</th>
                                        <th>Stock</th>
                                        <th>Opciones</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th></th>
                                        <th></th>                                
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th><b style="color: #D01040;">Stock Total:</b></th>
                                        <th>    <?php foreach ($uproducto as $stock) { ?>
                                            <b style="color: #003eff;"><?php echo $stock['stock']; ?></b>
                                            <?php } ?></th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                                <tbody>

                                    <?php foreach ($vproductos as $producto) { ?>
                                        <tr>
                                            <td><?php echo $producto['id_producto']; ?></td>
                                            <td><?php echo $producto['producto']; ?></td>                                    
                                            <td><?php echo $producto['sucursal']; ?></td>
                                            <td><?php echo $producto['almacen']; ?></td>                                    
                                            <td><?php echo $producto['umedida']; ?></td>
                                            <td><?php echo utf8_encode($producto['descripcion']); ?></td>
                                            <td><?php echo $producto['stock']; ?></td>
                                            <td>                                      
                                                <button type="button" title="Visualizar" onclick="productos_upload(<?php echo $producto["almacen"]; ?>)" class="btn btn-icon waves-effect btn-light" data-toggle="modal" data-target=".bs-example-modal-lg"> <i class="mdi mdi-cloud-upload"></i></button>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                            <?php
                        } else {
                            echo " <div class='alert alert-block '>
                                              <h4 class='alert-heading'> Informacion!!! </h4>
                                              En estos momentos no tiene ningun producto encontrado...
                                              </div>";
                        }
                        ?> 
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>



<center><div class="col-md-6">
        <div class="card card-outline-warning">
            <?php foreach ($uproducto as $uproductos) { ?>
                <div class="card-header">
                    <h4 class="m-b-0 text-white"><?php echo $uproductos['descripcion']; ?></h4></div>
                <div class="card">
                    <img class="card-img-top img-responsive" src="../assets/images/big/zapato.jpg" alt="Card image cap">
                    <div class="card-body">
                        <h4 class="card-title" style="color: #C7254E;"><?php echo $uproductos['producto']; ?></h4>
                        <h5 class="card-title"><b>Producto:</b> <?php echo $uproductos['id_producto']; ?> - <b>Stock:</b> <?php echo $uproductos['stock']; ?></h5>
                    </div>
                </div> 
            <?php } ?>

        </div>
    </div></center>
<div class="col-md-4">
    <div class="card">
        <div class="card-body">            
            <div  class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                <div id="my_modal_producto" class="modal-dialog modal-lg">

                </div>
            </div>         
        </div>
    </div>
</div>
