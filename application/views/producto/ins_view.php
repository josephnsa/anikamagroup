<!-- Start Page content -->
<div class="content">
    <div class="container-fluid">
        <!-- Row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header bg-info">
                        <h4 class="m-b-0 text-white">Busqueda Producto</h4>
                    </div>
                    <div class="card-body">
                        <form id="frm_registrar_producto" name="frm_registrar_producto">
                            <div class="form-body">                         
                                <div class="row p-t-20">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Codigo Producto</label>
                                            <div class="input-group mb-3">                                                                                               
                                                <input type="text" name="txt_codigo_producto" id="txt_codigo_producto" class="form-control" placeholder="Codigo" value="">
                                                <div class="input-group-prepend">
                                                    <button class="btn btn-info" type="button" 
                                                            onclick="buscar_producto()">
                                                        <i class="fa fa-search"></i> 
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>  
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Descripción</label>
                                            <div class="input-group mb-3">                                                                                               
                                                <input type="text" name="txt_desc_producto" id="txt_desc_producto" class="form-control" placeholder="Descripción" value="">
                                                <div class="input-group-prepend">
                                                    <button class="btn btn-info" type="button" 
                                                            onclick="buscar_producto()">
                                                        <i class="fa fa-search"></i> 
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/row-->                                                                
                            </div>
                        </form>                
                    </div>      
                    <div id="myModal">

                    </div>
                    <center><div id="preload"></div></center>
                </div>

            </div>
        </div>
        <!-- Row -->
    </div>
</div>
<!-- ============================================================== -->


