
<!DOCTYPE html>
<html lang="es">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Favicon icon -->
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo URL_IMG; ?>favicon.png">
        <title>Inicio - Anikama</title>
        <!-- Bootstrap Core CSS -->
        <link href="<?php echo URL_ASS; ?>node_modules/bootstrap/css/bootstrap.css" rel="stylesheet">
        <link href="<?php echo URL_ASS; ?>node_modules/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
        <!-- Footable CSS -->
        <link href="<?php echo URL_ASS; ?>node_modules/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo URL_ASS; ?>node_modules/footable/css/footable.core.css" rel="stylesheet">
        <link href="<?php echo URL_ASS; ?>node_modules/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
        <link href="<?php echo URL_ASS; ?>node_modules/multiselect/css/multi-select.css" rel="stylesheet" type="text/css" />
        <!-- Page CSS -->
        <link href="<?php echo URL_CSS; ?>pages/contact-app-page.css" rel="stylesheet">
        <link href="<?php echo URL_CSS; ?>pages/footable-page.css" rel="stylesheet">
        <!-- This page CSS -->
        <!-- chartist CSS -->
        <link href="<?php echo URL_ASS; ?>node_modules/morrisjs/morris.css" rel="stylesheet">
        <!--c3 CSS -->
        <link href="<?php echo URL_ASS; ?>node_modules/c3-master/c3.min.css" rel="stylesheet">
        <!--Toaster Popup message CSS -->
        <link href="<?php echo URL_ASS; ?>node_modules/toast-master/css/jquery.toast.css" rel="stylesheet">

        <!-- page CSS -->
        <link href="<?php echo URL_ASS; ?>node_modules/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css" />
        <!-- Custom CSS -->
        <link href="<?php echo URL_CSS; ?>style.css" rel="stylesheet">
        <!-- page css -->
        <link href="<?php echo URL_CSS; ?>pages/inbox.css" rel="stylesheet">
        <!-- Dashboard 1 Page CSS -->
        <link href="<?php echo URL_CSS; ?>pages/dashboard1.css" rel="stylesheet">

        <link href="<?php echo URL_JS; ?>scripts_uploadify/uploadify.css" rel="stylesheet">
        <!-- page css -->
        <link href="<?php echo URL_CSS; ?>pages/tab-page.css" rel="stylesheet">
        <!-- You can change the theme colors from here -->
        <link href="<?php echo URL_CSS; ?>colors/default.css" id="theme" rel="stylesheet">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

    <![endif]-->
    </head>

    <body class="fix-header fix-sidebar card-no-border">

               <?php
               if(!$this->session->userdata('usu_correo'))

                 redirect('')

                ?>





        <style type="text/css">
            .error {
                color:red !important;
            }
        </style>
        <!-- ============================================================== -->
        <!-- Preloader - style you can find in spinners.css -->
        <!-- ============================================================== -->

        <!-- ============================================================== -->
        <!-- Main wrapper - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <div id="main-wrapper">
            <!-- ============================================================== -->
            <!-- Topbar header - style you can find in pages.scss -->
            <!-- ============================================================== -->
            <header class="topbar">
                <nav class="navbar top-navbar navbar-expand-md navbar-light">
                    <!-- ============================================================== -->
                    <!-- Logo -->
                    <!-- ============================================================== -->
                    <div class="navbar-header">
                        <a class="navbar-brand" href="http://1ocalhost/guayano/welcome/index">
                            <!-- Logo icon --><b>
                                <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                                <!-- Dark Logo icon -->
                                <img style="width: 55px; height: 52px;" src="<?php echo URL_IMG; ?>anikama.png" alt="homepage" class="dark-logo" />
                                <!-- Light Logo icon -->
                                <img src="<?php echo URL_IMG; ?>anikama.png" alt="homepage" class="light-logo" />
                            </b>
                            <!--End Logo icon -->
                            <!-- Logo text --><span>
                                <!-- dark Logo text -->
                                <img style="width: 170px; height: 54px;" src="<?php echo URL_IMG; ?>anikamagroup.png" alt="homepage" class="dark-logo" />
                                <!-- Light Logo text -->
                                <!--<img src="<?php echo URL_IMG; ?>logo-light-text.png" class="light-logo" alt="homepage" /></span> </a>-->
                    </div>
                    <!-- ============================================================== -->
                    <!-- End Logo -->
                    <!-- ============================================================== -->
                    <div class="navbar-collapse">
                        <!-- ============================================================== -->
                        <!-- toggle and nav items -->
                        <!-- ============================================================== -->
                        <ul class="navbar-nav mr-auto">
                            <!-- This is  -->
                            <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up waves-effect waves-dark" href="javascript:void(0)"><i class="sl-icon-menu"></i></a> </li>
                            <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down waves-effect waves-dark" href="javascript:void(0)"><i class="sl-icon-menu"></i></a> </li>
                            <!-- ============================================================== -->
                            <!-- Search -->
                            <!-- ============================================================== -->

                        </ul>
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                        <ul   class="navbar-nav my-lg-0">

                            <li class="nav-item dropdown mega-dropdown"> <a class="nav-link dropdown-toggle waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i style="color: #92a8d1;" >Contactanos</i></a>

                            <!--   <i class="icon-Box-Close"></i> -->
                                <div class="dropdown-menu animated bounceInDown">
                                    <ul class="mega-dropdown-menu row">
                                        <li class="col-lg-3 col-xlg-2 m-b-30">
                                            <h4 class="m-b-20">Ubícanos</h4>


                                            <!--Google map-->
                                            <!--Google map-->
                                        <!-- <div id="map-container-google-1" class="z-depth-1-half map-container" style="height: 500px">
                                          <iframe src="https://maps.google.com/maps?q=anikamagroup&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0"
                                            style="border:0" allowfullscreen></iframe>
                                        </div> -->

    <!--Google Maps-->

                                            <div id="map-container-google-1" class="z-depth-1-half map-container" style="height: 200px">
                                              <iframe src="https://maps.google.com/maps?q=anikamagroup&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0"
                                              style="border:0" allowfullscreen></iframe>
                                            <style>
                                            .map-container{
                                              overflow:hidden;
                                              padding-bottom:56.25%;
                                              position:relative;
                                              height:0;
                                              }
                                              .map-container iframe{
                                              left:0;
                                              top:0;
                                              height:100%;
                                              width:100%;
                                              position:absolute;
                                              }
                                            </style>
                                            </div>
                                            <!-- <iframe
                                              ancho = "600"
                                              altura = "450"
                                              frameborder = "0" style = "border: 0"
                                              src = "https://www.google.com/maps/embed/v1/place?key= YOUR_API_KEY
                                                & q = Espacio + Aguja, Seattle + WA "pantalla completa>
                                            </iframe> -->
                                            <!-- <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                                                <div class="carousel-inner" role="listbox">
                                                    <div class="carousel-item active">
                                                        <div class="container"> <img class="d-block img-fluid" src="<?php echo URL_IMG; ?>big/img1.jpg" alt="First slide"></div>
                                                    </div>
                                                    <div class="carousel-item">
                                                        <div class="container"><img class="d-block img-fluid" src="<?php echo URL_IMG; ?>big/img2.jpg" alt="Second slide"></div>
                                                    </div>
                                                    <div class="carousel-item">
                                                        <div class="container"><img class="d-block img-fluid" src="<?php echo URL_IMG; ?>big/img3.jpg" alt="Third slide"></div>
                                                    </div>
                                                </div>
                                                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev"> <span class="carousel-control-prev-icon" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a>
                                                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next"> <span class="carousel-control-next-icon" aria-hidden="true"></span> <span class="sr-only">Next</span> </a>
                                            </div> -->
                                            <!-- End CAROUSEL -->
                                        </li>
                                        <li class="col-lg-3 m-b-30">
                                            <h4 class="m-b-20">Datos Informativos</h4>
                                            <!-- Accordian -->
                                            <div id="accordion" class="nav-accordion" role="tablist" aria-multiselectable="true">
                                                <div class="card">
                                                    <div class="card-header" role="tab" id="headingOne">
                                                        <h5 class="mb-0">
                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                                Av. Juan Pablo II 325 Of. 207 - Trujillo, Perú
                                                            </a>
                                                        </h5> </div>
                                                        <div class="card">
                                                            <div class="card-header" role="tab" id="headingThree">
                                                                <h5 class="mb-0">
                                                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                                        (044) 597 449
                                                                    </a>
                                                                </h5> </div>
                                                            <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                <div class="card-body"> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </div>
                                                            </div>
                                                        </div>
                                                </div>
                                                <div class="card">
                                                    <div class="card-header" role="tab" id="headingThree">
                                                        <h5 class="mb-0">
                                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                                 RPC: 969389976
                                                            </a>
                                                        </h5> </div>
                                                    <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                        <div class="card-body"> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </div>
                                                    </div>
                                                </div>
                                                <div class="card">
                                                    <div class="card-header" role="tab" id="headingThree">
                                                        <h5 class="mb-0">
                                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                                informes@anikamagroup.com
                                                            </a>
                                                        </h5> </div>
                                                    <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                        <div class="card-body"> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="col-lg-3  m-b-30">
                                            <h4 class="m-b-20">Contactanos</h4>

                                             <form method="post">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Ingresar nombre"> </div>
                                                <div class="form-group">
                                                    <input type="email" class="form-control" id="email" name="email" placeholder="Ingresar correo"> </div>

                                                    <div class="form-group">
                                                        <input type="text" class="form-control" id="asunto" name="asunto" placeholder="Ingresar asunto"> </div>
                                                <div class="form-group">
                                                    <textarea class="form-control" id="mensaje" name="mensaje" rows="3" placeholder="Ingresar mensaje"></textarea>
                                                </div>
                                                <button type="submit" class="btn btn-warning offset-2">Enviar</button>
                                            </form>
                                        </li>


                                       <?php
                                            header("Content-type: text/html;charset=\"utf-8\"");
                                            $error = ""; $mensajeExito = "";

                                            if ($_POST) {
                                                if ($_POST['email'] && filter_var($_POST["email"], FILTER_VALIDATE_EMAIL) === false) {
                                                    $error .= "E-mail no válido.<br>";
                                                }
                                                if ($error != "") {
                                                    $error = '<div class="alert alert-danger" role="alert"><p><b>Se generó un error:</b></p>' . $error . '</div>';
                                                } else {
                                                    $nombre = $_POST['nombre'];
                                                    $mail = $_POST['email'];
                                                    $asunto = $_POST['asunto'];
                                                    $mensajeC = $_POST['mensaje'];

                                                    $header = 'From: ' . $mail . " \r\n";
                                                    $header .= "X-Mailer: PHP/" . phpversion() . " \r\n";
                                                    $header .= "Mime-Version: 1.0 \r\n";
                                                    $header .= "Content-Type: text/plain";

                                                    $mensaje = "Este mensaje fue enviado por " . $nombre . ",\r\n";
                                                    $mensaje .= "Su e-mail es: " . $mail . " \r\n";
                                                    $mensaje .= "Asunto: " . $asunto . " \r\n";
                                                    $mensaje .= "Mensaje: " . $mensajeC . " \r\n";
                                                    $mensaje .= "Enviado el " . date('d/m/Y', time());

                                                    $para = 'cvasquez@anikamagroup.com';
                                                    $asunto = 'Mensaje de mi sitio web';

                                                    if (mail($para, $asunto, utf8_decode($mensaje), $header)) {
                                                        $mensajeExito = '<div class="alert alert-success" role="alert">Mensaje enviado con éxito :)</div>';
                                                    } else {
                                                        $error = '<div class="alert alert-danger" role="alert"><p><strong>Mensaje sin enviar :(</div>';
                                                    }
                                                }
                                            }
                                        ?>






                                        <li class="col-lg-3 col-xlg-4 m-b-30">
                                            <h4 class="m-b-20">Redes sociales</h4>
                                            <!-- List style -->
                                            <ul class="list-style-none">
                                              <!-- <a href="http://twitter.com/AQUI_TU_USUARIO" target="_blank"><img src="https://lh6.googleusercontent.com/--aIk2uBwEKM/T3nN1x09jBI/AAAAAAAAAs8/qzDsbw3kEm8/s32/twitter32.png" width=32 height=32 alt="Síguenos en Twitter" /></a> -->
                                              <!-- <a href="http://www.facebook.com/AQUI_TU_PAGINA" target="_blank"><img alt="Siguenos en Facebook" src="https://lh6.googleusercontent.com/-CYt37hfDnQ8/T3nNydojf_I/AAAAAAAAAr0/P5OtlZxV4rk/s32/facebook32.png" width=32 height=32  /></a> -->
                                              <!-- <a href="http://www.linkedin.com/TU_PERFIL_PUBLICO" target="_blank"><img alt="Siguenos en Linkedin" src="https://lh5.googleusercontent.com/-8C0OdSp_7ZA/T3nN0G_313I/AAAAAAAAAsU/6_Hbu6Of3qU/s32/linkedin32.png" width=32 height=32  /></a> -->



                                                <li><a href="https://www.facebook.com/AnikamaGroup/" target="_blank"><img alt="Siguenos en Facebook" src="https://lh6.googleusercontent.com/-CYt37hfDnQ8/T3nNydojf_I/AAAAAAAAAr0/P5OtlZxV4rk/s32/facebook32.png" width=20 height=20/> AnikamaGroup</a></li>
                                                <li><a href="https://twitter.com/anikamagroup?lang=es"><img src="https://lh6.googleusercontent.com/--aIk2uBwEKM/T3nN1x09jBI/AAAAAAAAAs8/qzDsbw3kEm8/s32/twitter32.png" width=20 height=20 alt="Siguenos en Twitter"/> AnikamaGroup</a>

                                                <li><a href="https://pe.linkedin.com/company/anikamagroup"><img alt="Siguenos en Linkedin" src="https://lh5.googleusercontent.com/-8C0OdSp_7ZA/T3nN0G_313I/AAAAAAAAAsU/6_Hbu6Of3qU/s32/linkedin32.png" width=20 height=20/> AnikamaGroup</a></li>

                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <!-- ============================================================== -->
                            <!-- End mega menu -->
                            <!-- ============================================================== -->
                            <!-- ============================================================== -->
                            <!-- Language -->
                            <!-- ============================================================== -->
                            <li class="nav-item dropdown">
                                <!--<a class="nav-link dropdown-toggle waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="flag-icon flag-icon-us"></i></a>-->
                                <div class="dropdown-menu dropdown-menu-right animated bounceInDown"> <a class="dropdown-item" href="#"><i class="flag-icon flag-icon-in"></i> India</a> <a class="dropdown-item" href="#"><i class="flag-icon flag-icon-fr"></i> French</a> <a class="dropdown-item" href="#"><i class="flag-icon flag-icon-cn"></i> China</a> <a class="dropdown-item" href="#"><i class="flag-icon flag-icon-de"></i> Dutch</a> </div>
                            </li>
                            <!-- ============================================================== -->
                            <!-- Profile -->
                            <!-- ============================================================== -->
                            <li class="nav-item dropdown u-pro">
                                <a class="nav-link dropdown-toggle waves-effect waves-dark profile-pic" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo URL_MAIN; ?>uploads/imagenes/<?php echo $this->session->userdata('usu_ruta_foto');?>" alt="user" class="" />
                                  <span class="hidden-md-down"><?php echo $this->session->userdata('usu_nombre'); ?> &nbsp;<i class="fa fa-angle-down"></i></span> </a>
                                <div class="dropdown-menu dropdown-menu-right animated flipInY">
                                    <ul class="dropdown-user">
                                        <li>
                                            <div class="dw-user-box">
                                                <div class="u-img"><img src="<?php echo URL_MAIN; ?>uploads/imagenes/<?php echo $this->session->userdata('usu_ruta_foto');?>" alt="user"></div>
                                                <div class="u-text">
                                                    <h4><?php echo $this->session->userdata('usu_correo'); ?></h4>
<!--                                                    <p class="text-muted">soporte@codelikeperu.com</p><a href="pages-profile.html" class="btn btn-rounded btn-danger btn-sm">View Profile</a></div>-->
                                                </div>
											</div>
                                        </li>
                                        <li role="separator" class="divider"></li>
                                        <li><a href="#"><i class="ti-user"></i> <?php echo $this->session->userdata('usu_nombre'); ?> </a></li>
                                        <li><a href="#"><i class="ti-email"></i> Inbox</a></li>
                                        <li role="separator" class="divider"></li>
                                        <li><a href="<?php echo URL_MAIN; ?>usuario/configCuenta"><i class="ti-settings"></i> Configurar</a></li>
                                        <li role="separator" class="divider"></li>
                                        <li><?php echo anchor('usuario/logout', '<i class="fa fa-power-off"></i> cerrar Sesion') ?>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
						</a>
					</div>
                </nav>
            </header>
            <!-- ============================================================== -->
            <!-- End Topbar header -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Left Sidebar - style you can find in sidebar.scss  -->
            <!-- ============================================================== -->
            <aside class="left-sidebar">
                <!-- Sidebar scroll-->
                <div class="scroll-sidebar">
                    <!-- Sidebar navigation-->
                    <nav class="sidebar-nav">
                        <ul id="sidebarnav">
                            <li class="nav-small-cap">--- PERSONAL</li>
                            <li> <a class="has-arrow waves-effect waves-dark" href="<?php echo URL_MAIN; ?>welcome/index" aria-expanded="false"><i class="icon-Car-Wheel"></i><span class="hide-menu">Dashboard <span class="label label-rounded label-danger">4</span></span></a>

                            </li>
                            <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="icon-Double-Circle"></i><span class="hide-menu">Apps</span></a>
                                <ul aria-expanded="false" class="collapse">
                                    <li><a href="<?php echo URL_MAIN; ?>producto/producto">Preparacion Tablas Maestras</a></li>
                                    <!--                                <li><a href="app-chat.html">Chat app</a></li>
                                                                    <li><a href="app-ticket.html">Support Ticket</a></li>-->
                                </ul>
                            </li>
                        </ul>
                    </nav>
                    <!-- End Sidebar navigation -->
                </div>
                <!-- End Sidebar scroll-->
            </aside>
            <!-- ============================================================== -->
            <!-- End Left Sidebar - style you can find in sidebar.scss  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Page wrapper  -->
            <!-- ============================================================== -->
            <div class="page-wrapper">
                <!-- ============================================================== -->
                <!-- All Jquery -->
                <!-- ============================================================== -->
                <script src="<?php echo URL_ASS; ?>node_modules/jquery/jquery.min.js"></script>
                <!-- Bootstrap popper Core JavaScript -->
                <script src="<?php echo URL_ASS; ?>node_modules/bootstrap/js/popper.min.js"></script>
                <script src="<?php echo URL_ASS; ?>node_modules/bootstrap/js/bootstrap.min.js"></script>
                <!-- slimscrollbar scrollbar JavaScript -->
                <script src="<?php echo URL_ASS; ?>node_modules/ps/perfect-scrollbar.jquery.min.js"></script>
                <!--Wave Effects -->
                <script src="<?php echo URL_JS; ?>waves.js"></script>
                <!--Menu sidebar -->
                <script src="<?php echo URL_JS; ?>sidebarmenu.js"></script>
                <!--Custom JavaScript -->
                <!--stickey kit -->
                <script src="<?php echo URL_ASS; ?>node_modules/sticky-kit-master/dist/sticky-kit.min.js"></script>
                <script src="<?php echo URL_ASS; ?>node_modules/sparkline/jquery.sparkline.min.js"></script>
                <!-- ============================================================== -->

                <!--Custom JavaScript -->
                <script src="<?php echo URL_JS; ?>custom.min.js"></script>
                <!-- ============================================================== -->
                <!-- Footable -->
                <script src="<?php echo URL_ASS; ?>node_modules/select2/dist/js/select2.full.min.js" type="text/javascript"></script>
                <script src="<?php echo URL_ASS; ?>node_modules/footable/js/footable.all.min.js"></script>
                <script src="<?php echo URL_ASS; ?>node_modules/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
                <script type="text/javascript" src="<?php echo URL_ASS; ?>node_modules/multiselect/js/jquery.multi-select.js"></script>

                <!--FooTable init-->
                <script src="<?php echo URL_JS; ?>footable-init.js"></script>
                <!-- Popup message jquery -->
                <script src="<?php echo URL_ASS; ?>node_modules/toast-master/js/jquery.toast.js"></script>
                <script src="<?php echo URL_JS; ?>toastr.js"></script>
                <!-- Custom Theme JavaScript -->
                <!-- ============================================================== -->
                <script src="<?php echo URL_ASS; ?>node_modules/switchery/dist/switchery.min.js"></script>

                <!-- Style switcher -->
                <!-- ============================================================== -->
                <script src="<?php echo URL_ASS; ?>node_modules/styleswitcher/jQuery.style.switcher.js"></script>


                <!-- This is data table -->
                <script src="<?php echo URL_ASS; ?>node_modules/datatables/jquery.dataTables.min.js"></script>
                <!-- start - This is for export functionality only -->
                <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
                <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
                <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
                <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
                <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
                <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
                <!-- end - This is for export functionality only -->

                <!-- This page plugins -->
                <!-- ============================================================== -->
                <!--morris JavaScript -->
                <script src="<?php echo URL_ASS; ?>node_modules/raphael/raphael-min.js"></script>
                <script src="<?php echo URL_ASS; ?>node_modules/morrisjs/morris.min.js"></script>
                <!--c3 JavaScript -->
                <script src="<?php echo URL_ASS; ?>node_modules/d3/d3.min.js"></script>
                <script src="<?php echo URL_ASS; ?>node_modules/c3-master/c3.min.js"></script>
                <!-- Chart JS -->
<!--                <script src="<?php echo URL_ASS; ?>node_modules/Chart.js/chartjs.init.js"></script>
                <script src="<?php echo URL_ASS; ?>node_modules/Chart.js/Chart.min.js"></script>-->
                <script src="<?php echo URL_JS; ?>dashboard1.js"></script>
                <!-- ============================================================== -->
                <!-- Funciones añadidas-->
                <script src="<?php echo URL_ASS; ?>node_modules/Bootstrap-3-Typeahead-master/bootstrap3-typeahead.min.js" type="text/javascript"></script>
                <script src="<?php echo URL_JS; ?>jsPrincipal.js"></script>
                <script src="<?php echo URL_JS; ?>jsValidacionGeneral.js"></script>
                <script src="<?php echo URL_PLUG; ?>jquery-validation/js/jquery.validate.min.js"></script>
                <!--                Custom JavaScript
                                <script src="<?php echo URL_JS; ?>custom.min.js"></script>
                                <script src="<?php echo URL_JS; ?>validation.js"></script>                -->
