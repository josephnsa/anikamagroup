
<link href="<?php echo URL_CSS ; ?>configCuenta.css" rel="stylesheet">
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <?php $this->load->view('dashboard/header'); ?>
    <script src="https://unpkg.com/gijgo@1.9.11/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.11/css/gijgo.min.css" rel="stylesheet" type="text/css" />



 <input  class="form-control" id="txt_idImple" hidden name="txt_idImple" type="text" value="<?php print_r ($data); ?>">

 
 
 
 
 <div class="container">
  <div class="view-account">
      <section class="module">
          <div class="module-inner">
              <div class="content-panel">
     
    <div class="text-center">
         <h1>Detalle Proyecto</h1>
    </div>
   
     <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <!-- <h3 class="panel-title">Cabecera</h3> -->
					<hr>
                    <div class="row">

                        <div class="col-md-5" style="background-color:white;">
                            <div class="form-group">
                                <label for="cliente" class="col-sm-2 control-label">CODIGO</label>
                                <div class="col-sm-8">
                                    <input class="form-control" id="txt_codigo" name="txt_codigo" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7" style="background-color:white;">
                            <div class="form-group">

                                <label for="fecha" class="col-sm-2 control-label">DESCRIPCIÓN</label>
                                <div class="col-sm-10">
                                    <input class="form-control" id="txt_descripcion" name="txt_descripcion"   type="text">
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                      <div class="col-md-6" style="background-color:white;">
                          <div class="form-group">
                              <label for="cliente" class="col-sm-2 control-label">CLIENTE</label>
                              <div class="col-sm-10">
                                  <input class="form-control" id="txt_cliente" name="txt_cliente"   type="text">
                              </div>
                          </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-5" style="background-color:white;">
                          <div class="form-group">
                              <label for="cliente" class="col-sm-4 control-label">Fecha de Inicio:</label>
                              <div class="col-sm-8">
                                <input class="form-control" id="fecha_ini" name="fecha_ini"   type="text">
                              
                              </div>
                          </div>
                      </div>
                      <div class="col-md-6" style="background-color:white;">
                          <div class="form-group">
                              <label for="cliente" class="col-sm-4 control-label">Fecha de Termino:</label>
                              <div class="col-sm-8">
                                <input class="form-control" id="fecha_ter" name="fecha_ter"   type="text">
                              </div>
                              
                               <div class="form-group">
					
 				</div>
                          </div>
                      </div>
                      </div>
                   <!-- row -->
                    <br>
				</div> <!-- panel heading -->
				
               <form action="<?php echo URL_MAIN;?>gantt" method="POST"><input type="hidden" name="txt_id" value="<?php print_r ($data); ?>"><button type="submit" class="btn btn-success">Cargar Gantt</button></form>
            <form action="<?php echo URL_MAIN;?>responsable/responsable" method="POST"><input type="hidden" name="txt_id" value="<?php print_r ($data); ?>"><button type="submit" class="btn btn-primary">Agregar Nuevo Responsable</button></form>
             </div> <!-- panel -->
        </div> <!-- col -->
      </div> <!-- row -->
    </div>
  </div>
</section>
</div>
</div>
<script type="text/javascript">
    
    $(document).ready(function () {        
    var baseurl = "<?php echo base_url();?>";
        var id = $('#txt_idImple').val();
        $.post(baseurl+"implementacion/detalle/getDetalle",
        {id : id},       
        function(data){
           
		var obj = JSON.parse(data);
			
		var output = '';
		$.each(obj, function(i,item){
            
            $('#txt_codigo').val(item.codigo);
            $('#txt_descripcion').val(item.descripcion);
            $('#txt_cliente').val(item.razon_social_ter);
			$('#fecha_ini').val(item.fecha_ini);
			$('#fecha_ter').val(item.fecha_ter);
			
			output += 
				'<tr>' +
				'	<td>'+item.actividad+'</td>' +          
				'	<td>'+item.nombreCompleto+'</td>' +
                '	<td>'+item.description+'</td>' +
                '	<td>'+item.fecha_ini+'</td>' +
                '	<td>'+item.fecha_ter+'</td>' +
		        '  	<td><a href="#" title="Editar informacion" data-toggle="modal" data-target="#modeldefResponsales" onClick="defResponsable(\''+item.id+'\',\''+item.codigo+'\',\''+item.actividad+'\');"><input type="submit" class="btn btn-warning btn-sm" value="Actualizar"></a></td>' +
		        '</tr>';
		});
		$('#tblListDetalle tbody').append(output);

	});
		
		
		//Obtener lista de Responsables
		$.post(baseurl+"responsable/responsable/getDetalle",
        {id : id},     
		  function(data){
           
		var obj = JSON.parse(data);
			
		var output = '';
		$.each(obj, function(i,item){
            
		$("#selecRespo").append('<option value='+item.idUsuarios+'>'+item.nombreCompleto+'</option>');

		});
		

	});
		
		
	defResponsable = function(id, codigo, actividad){
	
	$('#mtxtCodigo').val(codigo);
	$('#mhdnIdActividad').val(id);
	$('#mtxtDescripcion').val(actividad);
	
	}
	
	 $('#mbtnAgregarResponsable').click(function(){
	var id = $('#mhdnIdActividad').val();
	var idResponsable = $('#selecRespo').val();
		
		 
	
	$.post(baseurl+"implementacion/detalle/updateTaskResponsable",	
	{
		mhdnIdA:id,
		mtxtidResponsable:idResponsable
	},			
	function(data){
		if (data == 1) {
			alert('Se Agrego el Responsable Correctamente!');
			$('#mbtnCerrarModal').click();

			location.reload();
		}
	});
});  
	
	
	
});
</script>
    
        
                <!-- container -->
<?php $this->load->view('dashboard/footer'); ?>
