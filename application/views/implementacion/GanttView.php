<script src="./static/lib/dhtmlxGantt/codebase/dhtmlxgantt.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" href="./static/lib/dhtmlxGantt/codebase/dhtmlxgantt.css" type="text/css" media="screen" title="no title" charset="utf-8">

<?php $this->load->view('dashboard/header'); ?>


<div id="gantt_here" style='width:100%; height:100%;'></div>

<script type="text/javascript">
    gantt.config.xml_date = "%Y-%m-%d %H:%i:%s";
    gantt.config.step = 1;
    gantt.config.scale_unit= "day";
    gantt.init("gantt_here", new Date(2010,7,1), new Date(2010,8,1));
    gantt.load("./gantt/data", "xml");

    var dp = new dataProcessor("./gantt/data");
    dp.init(gantt);

</script>
<?php $this->load->view('dashboard/footer'); ?>