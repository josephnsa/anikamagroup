
<link href="<?php echo URL_CSS ; ?>configCuenta.css" rel="stylesheet">
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <?php $this->load->view('dashboard/header'); ?>
    <script src="https://unpkg.com/gijgo@1.9.11/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.11/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    <title>Gestionar Proyecto</title>

<div class="container">
    <div class="view-account">
        <section class="module">
            <div class="module-inner">
     <div class="content-panel">
<form action="<?php echo URL_MAIN; ?>implementacion/implementacion/guardar" method="post">
    <fieldset class="fieldset">
    <h3 class="fieldset-title">Implementacion del Proyecto</h3>


  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="inputEmail4">RUC</label>
      <input type="text"  maxlength="11" id="txt_ruc" class="form-control" name="txt_ruc" id="txt_ruc" placeholder=""   required>
    </div>
  </div>


  <div class="form-group">
    <label for="inputAddress">RAZÓN SOCIAL</label>
    <input type="text" class="form-control" id="txt_razon" name="txt_razon" id="txt_razon" placeholder="" required>
  </div>
  <div class="form-group ">
    <label for="inputAddress2">REPRESENTANTE</label>
    <input type="text" class="form-control" name="txt_repre" id="txt_repre" placeholder=""required>
  </div>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="inputCity">Email</label>
      <input type="email" class="form-control" name="txt_email" id="txt_email" required>
    </div>

  </div>
        </fieldset>
    <fieldset class="fieldset">
    <h3 class="fieldset-title">Datos de Proyecto</h3>

      <div class="form-row">
        <div class="form-group col-md-6">
        <label for="inputEmail4">CODIGO</label>
          <input type="text"  id="txt_codigo" class="form-control" name="txt_codigo" placeholder=""/>
        </div>
      </div>


    <div class="form-group">
      <label for="inputEmail4">DESCRIPCION</label>
      <input type="text" class="form-control" name="txt_descri" id="txt_descri" placeholder="" required>
    </div>


    <div class="form-row">
    <div class="form-group col-md-2">
      <label for="inputState">Fecha de Inicio</label>
      <input id="datepicker2" name="txt_fecha_ini" id="txt_fecha_ini" class="form-control" required/>
    <script>
        $('#datepicker2').datepicker({
            format: 'dd/mm/yyyy',
            uiLibrary: 'bootstrap4'
        });
    </script>
    </div>
    <div class="form-group col-md-2">
      <label for="inputState">Fecha de Termino</label>
      <input id="datepicker" name="txt_fecha_ter" id="txt_fecha_ter" data-date-format="dd/mm/yyyy" class="form-control" required/>
    <script>
        $('#datepicker').datepicker({
            format: 'dd/mm/yyyy',
            uiLibrary: 'bootstrap4'

        });
    </script>
    </div>
    </div>

    </fieldset>

  <button type="submit" id="btnImplementacion" class="btn btn-primary">Crear Proyecto</button>

  <!-- <a href="detalle.php" class="btn btn-primary">Crear Proyecto</a> -->

</form>
</div>
            </div>
        </section>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function () {

    var baseurl = "<?php echo base_url();?>";

        //METODO PARA MOSTRAR INFORMACION POR RUC

        $.post(baseurl+"implementacion/implementacion/getCodigo",
               function(data){
			//alert(data);
			var obj = JSON.parse(data);

//            alert(obj[0].codigo);
            if(obj[0].codigo==null){
                $('#txt_codigo').val("2019 - 01");
            }else{
                if(obj[0].codigo>9){
                $('#txt_codigo').val("2019 - "+obj[0].codigo);
                }else{
                   $('#txt_codigo').val("2019 - 0"+obj[0].codigo);
                }
            }
        });

    $('#txt_ruc').keyup(function(){
	//alert($('#txtBuscarCiudad').val());

	var text = $('#txt_ruc').val();
	var lt = $('#txt_ruc').val().length;
	// alert(lt);
	if (lt >= 11) {
		$.post(baseurl+"implementacion/implementacion/getDatos",
		{texto : text},
		function(data){
			//alert(data);
			var obj = JSON.parse(data);

	    $('#txt_razon').val(obj[0].emp_descripcion);
//        $('#txt_repre').val(obj.nombres);

		});
	}else if(lt == 0){
        $mensaje="";
		$('#txt_razon').val($mensaje);
	}

    });


});


</script>
<?php $this->load->view('dashboard/footer'); ?>
