<link href="<?php echo URL_CSS ; ?>configCuenta.css" rel="stylesheet">
<?php $this->load->view('dashboard/header'); ?>
<div class="container">
 <div class="view-account">
     <section class="module">
         <div class="module-inner">
             <div class="content-panel">

   <div class="text-center">
        <h1>Proyectos</h1>
   </div>

    <div class="row">
       <div class="col-md-12">
           <div class="panel panel-default">

              <div class="table-responsive m-t-20 no-wrap">
                                   <table class="table table-striped vm no-th-brd pro-of-month" id="tblListDetalle" table-striped>
                                       <thead>
                                           <tr>

                                               <th>Codigo</th>
                                               <th>Descripción</th>
                                               <th>Fecha Inicio</th>
                                               <th>Fecha Termino</th>

                                               <th></th>
                                           </tr>
                                       </thead>
                                       <tbody>

                                       </tbody>
                                   </table>
                               </div>

                   <a  href="<?php echo URL_MAIN;?>implementacion/implementacion"><button type="submit" class="btn btn-success">Crear Nuevo Proyecto</button></a>


            </div> <!-- panel -->
       </div> <!-- col -->
     </div> <!-- row -->
   </div>
 </div>
</section>
</div>

</div>

 <!-- MODAL ELIMINAR -->

<div class="modal fade" id="modalEliminartPersona" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
 <div class="modal-dialog modal-lg" role="document">
   <div class="modal-content">

     <div class="modal-header bg-blue">
       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
       <h4 class="modal-title" id="myModalLabel">ELIMINAR IMPLEMENTACIÓN</h4>
     </div>

     <div class="modal-body">
       <form class="form-horizontal">
         <!-- parametros ocultos -->
         <input type="hidden" id="mhdnIdImpE">

     <div class="box-body">
           <div class="form-group">
               <label class="col-sm-3 control-label">Codigo</label>
               <div class="col-sm-9">
                 <input type="text" name="mtxtCodigoE" class="form-control" id="mtxtCodigoE" placeholder="">
               </div>
           </div>

           <div class="form-group">
               <label class="col-sm-3 control-label">Descripcion</label>
               <div class="col-sm-9">
                 <input type="text" name="mtxtDescripcionE" class="form-control" id="mtxtDescripcionE" value="" >
               </div>
           </div>

     </div>
     </form>
     </div>

     <div class="modal-footer">
       <button type="button" class="btn btn-default" id="mbtnCerrarModal" data-dismiss="modal">Cancelar</button>
       <button type="button" class="btn btn-info" id="mbtnDeleteImp">Eliminar </button>
     </div>
   </div>
 </div>
</div>
                                   <!-- MODAL ACTUALIZAR -->

<div class="modal fade" id="modalEditPersona" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
 <div class="modal-dialog modal-lg" role="document">
   <div class="modal-content">

     <div class="modal-header bg-black">
       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
       <h4 class="modal-title" id="myModalLabel">ACTUALIZAR IMPLEMENTACIÓN</h4>
     </div>

     <div class="modal-body">
       <form class="form-horizontal">
         <!-- parametros ocultos -->
         <input type="hidden" id="mhdnIdA">

     <div class="box-body">
           <div class="form-group">
               <label class="col-sm-3 control-label">Codigo</label>
               <div class="col-sm-9">
                 <input type="text" name="mtxtCodigoA" class="form-control" id="mtxtCodigoA" placeholder="">
               </div>
           </div>

           <div class="form-group">
               <label class="col-sm-3 control-label">Descripción</label>
               <div class="col-sm-9">
                 <input type="text" name="mtxtDescripcionA" class="form-control" id="mtxtDescripcionA" value="" >
               </div>
           </div>

           <div class="form-group">
               <label class="col-sm-3 control-label">Fecha</label>
               <div class="col-sm-9">
                 <input type="text" name="mtxtFechaIniA" class="form-control" id="mtxtFechaIniA">
               </div>
           </div>

           <div class="form-group">
               <label class="col-sm-3 control-label">Fecha</label>
               <div class="col-sm-9">
                 <input type="text" name="mtxtFechaTerA" class="form-control" id="mtxtFechaTerA">
               </div>
           </div>

     </div>
     </form>
     </div>

     <div class="modal-footer">
       <button type="button" class="btn btn-default" id="mbtnCerrarModal" data-dismiss="modal">Cancelar</button>
       <button type="button" class="btn btn-info" id="mbtnAcualizarIm">Actualizar </button>
     </div>
   </div>
 </div>
</div>




<script type="text/javascript">

   $(document).ready(function () {



       var baseurl = "<?php echo base_url();?>";
       $.post(baseurl+"implementacion/inicio/getImpleXusuario",
       function(data){

   var obj = JSON.parse(data);
   var output = '';
   $.each(obj, function(i,item){
            $('#txt_codigo').val(item.id);

     output +=
       '<tr>' +

       '	<td>'+item.codigo+'</td>' +
       '	<td>'+item.descripcion+'</td>' +
       '	<td>'+item.fecha_ini+'</td>' +
               '	<td>'+item.fecha_ter+'</td>' +


               //                '	<td>'+item.razon_social_ter+'</td>' +
                               '  	<td  ><form action="<?php echo URL_MAIN;?>implementacion/detalle" method="POST"><input type="hidden" name="txt_id" value="'+item.id+'"><input style="height:32px; margin-top: 15px;" type="submit" class="btn btn-primary btn-sm" value="Detalle"></form></td>' +
                               '  	<td><a href="#" title="Editar informacion" data-toggle="modal" data-target="#modalEliminartPersona" onClick="eliminar(\''+item.id+'\',\''+item.codigo+'\',\''+item.descripcion+'\');"><input type="submit" class="btn btn-danger btn-sm" value="Eliminar"></a></td>' +
                                '  	<td><a href="#" title="Editar informacion" data-toggle="modal" data-target="#modalEditPersona" onClick="actualizar(\''+item.id+'\',\''+item.codigo+'\',\''+item.descripcion+'\',\''+item.fecha_ini+'\',\''+item.fecha_ter+'\');"><input type="submit" class="btn btn-warning btn-sm" value="Actualizar"></a></td>' +





           '</tr>';
   });
   $('#tblListDetalle tbody').append(output);

 });

   actualizar = function(id, codigo, descripcion, fecha_ini, fecha_ter){
 $('#mhdnIdA').val(id);
 $('#mtxtCodigoA').val(codigo);
 $('#mtxtDescripcionA').val(descripcion);
 $('#mtxtFechaIniA').val(fecha_ini);
 $('#mtxtFechaTerA').val(fecha_ter);

};

   eliminar = function(id, codigo, descripcion){
 $('#mhdnIdImpE').val(id);
 $('#mtxtCodigoE').val(codigo);
 $('#mtxtDescripcionE').val(descripcion);


};

   $('#mbtnAcualizarIm').click(function(){
 var id = $('#mhdnIdA').val();
 var codigo = $('#mtxtCodigoA').val();
 var descripcion = $('#mtxtDescripcionA').val();
 var fecha_ini = $('#mtxtFechaIniA').val();
 var fecha_ter = $('#mtxtFechaTerA').val();
 $.post(baseurl+"implementacion/implementacion/actualizarImp",
 {
   mhdnIdA:id,
   mtxtCodigoA:codigo,
   mtxtDescripcionA:descripcion,
   mtxtFechaIniA:fecha_ini,
   mtxtFechaTerA:fecha_ter
 },
 function(data){
   if (data == 1) {
     alert('Se Actualizo correctamente!');
     $('#mbtnCerrarModal').click();

     location.reload();
   }
 });
});


 $('#mbtnDeleteImp').click(function(){
 var id = $('#mhdnIdImpE').val();

 $.post(baseurl+"implementacion/implementacion/eliminarImp",
 {
   mhdnIdImpE:id

 },
 function(data){
   if (data == 1) {
     alert('Elimino Correctamente');
     $('#mbtnCerrarModal').click();

     location.reload();
   }
 });
});


});
</script>

<?php $this->load->view('dashboard/footer'); ?>
