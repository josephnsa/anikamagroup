<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Producto extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('producto/producto_model');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->helper('url');
    }

    public function index() {
//        $this->loaders->verificaAcceso('W');
        $data['titulo'] = 'Registro de Producto';
        $this->load->view('producto/panel_view', $data);
    }


    function buscar_productoxcodigo($valor, $valor2) {
        $resultado = str_replace("_", "/", $valor2);
        $tipo = $this->session->userdata('estipo');
        if ($tipo == '1') {
            $data['uproducto'] = $this->producto_model->buscar_producto_unico($valor, $resultado);
            $data['vproductos'] = $this->producto_model->buscar_productoxcodigo($valor, $resultado);
            $this->load->view('producto/qry_view_v', $data);
        } else {
            $data['uproducto'] = $this->producto_model->buscar_producto_unico($valor, $resultado);
            $this->load->view('producto/qry_view_v', $data);
        }
    }

}


 
 
 