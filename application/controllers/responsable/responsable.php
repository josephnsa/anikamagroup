<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Responsable extends CI_Controller {

      function __construct() {
        parent::__construct();
//        $this->load->model('producto/producto_model');
        $this->load->helper('form');
//        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->load->model('usuario_model');
        $this->load->model('responsable/responsable_model');

    }

         public function index() {
			 	$data = $this->input->post('txt_id');
                $this->load->view('responsable/responsable',compact("data"));
      }


      public function getDatos(){
        $text = $this->input->post('texto');
        $url_RUC= "http://demosprinter.anikamagroup.com/consulta-dni/".$text;
        $url_ruc_json=file_get_contents($url_RUC);
        $url_ruc_array = json_decode($url_ruc_json,true);

        echo json_encode($url_ruc_array);

      }

         function guardarResp() {
		  $imagen = "usuariodefault.png";
		  $fecha = date('Y-m-d');

          $param['dni'] = $this->input->post('txt_dni');
          $param['usu_nombre'] = $this->input->post('txt_nombre');
          $param['usu_apellidos'] = $this->input->post('txt_apellidos');
          $param['usu_correo'] = $this->input->post('txt_email');
          $param['usu_clave'] = sha1($this->input->post('txt_contraseña'));
          $param['celular'] = $this->input->post('txt_telefono');
		  $param['usu_ruta_foto'] = $imagen;
		  $param['usu_fecha_creacion'] = $fecha;
		  $param['usu_estado'] = "H";
		  $param['idimple'] = $this->input->post('txt_idImple');



          $this->responsable_model->registrar($param);
			 redirect('responsable/responsable');
		 }

	public function getDetalle(){
		$resultado = $this->responsable_model->detalle();
		echo json_encode($resultado);

    	}

    public function getActividadesXresponsble(){
		$resultado = $this->responsable_model->ActividadesXresponsable();
		echo json_encode($resultado);

    	}


      public function ActividadePredecesora(){
        $id = $this->input->post('id');
		$resultado = $this->responsable_model->ActividadePredecesora($id);
		echo json_encode($resultado);

    	}
    


    }
