<?php

if (!defined('BASEPATH'))
    exit('No esta permitido el acceso');

class Usuario extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('form');
          $this->load->library('encryption');
        $this->load->library('form_validation');
        $this->load->library('email');
		    $this->load->library('session');
        $this->load->model('usuario_model');

    }

    function login() {
        $data['title'] = 'Login Usuario';
        $this->load->view("usuario/login1", $data);
    }

	function cargarLogin(){
		$this->load->view("usuario/login1");
	}

	function registro(){
		$this->load->view("usuario/registro");
	}
    function logout() {
        $this->session->sess_destroy();
        redirect("usuario/login");
    }

	function configCuenta(){
		$this->load->view("usuario/configCuenta");
	}

    function autenticar() {

      $this->form_validation->set_rules('txt_usuario', 'Usuario', 'required|valid_email|trim');
      $this->form_validation->set_rules('txt_password', 'Contraseña', 'required|trim');
      $this->form_validation->set_message('required', 'El campo %s es requerido');
      $this->form_validation->set_message('valid_email','El campo %s debe ser un email valido');


        if ($this->form_validation->run() == true) {
            $user = $this->input->post('txt_usuario');
            $pass = sha1($this->input->post('txt_password'));
            $data = array('user' => $user, 'pass' => $pass);


            $login = $this->usuario_model->autenticarResponsable($data);

            if ($login) {
                $data = array(
                'esta_logeado' => true,
                'idUsu' => $this->usuario_model->getNUsuId(),
				'usu_clave' => $this->usuario_model->getClave(),
                'usu_nombre' => $this->usuario_model->getNombre(),
                'usu_apellidos' => $this->usuario_model->getApellidos(),
				'usu_estado' =>$this->usuario_model->getEstado(),
				'usu_ruta_foto' =>$this->usuario_model->getRutaFoto(),
                'usu_correo' => $this->usuario_model->getCorreo(),
				'celular' => $this->usuario_model->getCelular(),
				'usu_fecha_creacion' =>$this->usuario_model->getFechaCreacion(),
                //'rol_nombre' => $this->usuario_model->getIdRol()
                );
                $this->session->set_userdata($data);
				$this->session->userdata($data);

                redirect('implementacion/inicio');
            }

            else {

                $login2 = $this->usuario_model->autenticarz($data);
                 if ($login2) {
                $data = array(
                'esta_logeado' => true,
                'idUsu' => $this->usuario_model->getNUsuId(),
				'usu_clave' => $this->usuario_model->getClave(),
                'usu_nombre' => $this->usuario_model->getNombre(),
                'usu_apellidos' => $this->usuario_model->getApellidos(),
				'usu_estado' =>$this->usuario_model->getEstado(),
				'usu_ruta_foto' =>$this->usuario_model->getRutaFoto(),
                'usu_correo' => $this->usuario_model->getCorreo(),
				'celular' => $this->usuario_model->getCelular(),
				'usu_fecha_creacion' =>$this->usuario_model->getFechaCreacion(),
                'rol_nombre' => $this->usuario_model->getIdRol()
                );
                $this->session->set_userdata($data);
				$this->session->userdata($data);

                redirect('implementacion/inicio');
            }else {
              $data["mensaje"]="Validación incorrecta";
              $this->load->view("usuario/login1", $data);
        }

            }
        } else {
          $data["mensaje"]="Validación incorrecta";
          $this->load->view("usuario/login1", $data);
        }
    }

        public function registrar() {

        $fecha = date('Y-m-d');
        $imagen = "usuariodefault.png";

            $param['usu_clave'] = sha1($this->input->post('txt_password'));
            $param['usu_nombre'] = $this->input->post('txt_nombre');
            $param['usu_apellidos'] =$this->input->post('txt_apellidos');
            $param['usu_estado'] = "H";
            $param['usu_ruta_foto'] = $imagen;
            $param['usu_correo'] =$this->input->post('txt_email');
            $param['usu_fecha_creacion'] = $fecha;
            $param['celular'] = $this->input->post('txt_celular');
            $param['rol_id'] = "1";

            echo $this->usuario_model->insertar($param);



        }

     public function actualizarDatos(){

       $config['upload_path'] = './uploads/imagenes/';
           $config['allowed_types'] = 'gif|jpg|png';
           $config['max_size'] = '2048';
           $config['max_width'] = '2024';
           $config['max_height'] = '2008';

           $this->load->library('upload',$config);

           if (!$this->upload->do_upload("fileImagen")) {
               $this->actualizarContra();
       }else {

           $file_info = $this->upload->data();


           $imagen = $file_info['file_name'];

           $param['usu_nombre'] = $this->input->post('txt_nombre');
           $param['usu_apellidos'] =$this->input->post('txt_apellidos');
           $param['usu_ruta_foto'] = $imagen;
           $param['usu_correo'] =$this->input->post('txt_email');
           $param['celular'] =$this->input->post('txt_celular');


              $this->usuario_model->actualizarDatos($param);
              $this->session->set_userdata($param);
              redirect('usuario/configCuenta');

       }
     }
     public function actualizarContra(){
       $param['usu_nombre'] = $this->input->post('txt_nombre');
       $param['usu_apellidos'] =$this->input->post('txt_apellidos');

       $param['usu_correo'] =$this->input->post('txt_email');
       $param['celular'] =$this->input->post('txt_celular');
       $this->usuario_model->actualizarDatos2($param);
       $this->session->set_userdata($param);
       redirect('usuario/configCuenta');
     }

}
