<?php


if (!defined('BASEPATH'))
    exit('No esta permitido el acceso');

class Ejemplo extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->model('implementacion/implementacion_model');
    }

    public function index(){

        
    $this->load->view('implementacion/ejemplo');
        
}

    }

