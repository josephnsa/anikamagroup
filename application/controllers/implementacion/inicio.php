<?php

if (!defined('BASEPATH'))
    exit('No esta permitido el acceso');

class Inicio extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->model('implementacion/implementacion_model');
        $this->load->model('responsable/responsable_model');
    }

    public function index() {
      	$data  = $this->session->userdata('rol_nombre');
		 
		 
		 if($data == 'Consultor'){
			 
        	$this->load->view('implementacion/inicio');
		}else {
			$this->load->view('implementacion/actividadesResponsable');
		 }
      
    }
    
     public function getImpleXusuario(){
		
			 $resultado = $this->implementacion_model->impleXusuario();
			 echo json_encode($resultado); 

    }
    
    
    
    
    

}