<?php

if (!defined('BASEPATH'))
    exit('No esta permitido el acceso');

class Implementacion extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->model('implementacion/implementacion_model');
        $this->load->model('terceros/terceros_model');
		$this->load->model('responsable/responsable_model');
    }

    public function index() {

        $data['titulo'] = 'Implementacion de Proyecto';
        $this->load->view('implementacion/implementacion');
    }

    public function getDatos(){

        $text = $this->input->post('texto');
        $url_RUC= "http://demosprinter.anikamagroup.com/consulta_ruc/".$text;
        $url_ruc_json=file_get_contents($url_RUC);
        $url_ruc_array = json_decode($url_ruc_json,true);

        echo json_encode($url_ruc_array);
    }

    public function getCodigo(){

		$resultado = $this->implementacion_model->codigoCorre();
		echo json_encode($resultado);
    }


    public function actualizarImp(){
        $param['id'] = $this->input->post('mhdnIdA');
		$param['codigo'] = $this->input->post('mtxtCodigoA');
		$param['descripcion'] = $this->input->post('mtxtDescripcionA');
		$param['fecha_ini'] = $this->input->post('mtxtFechaIniA');
		$param['fecha_ter'] = $this->input->post('mtxtFechaTerA');

		echo $this->implementacion_model->actualizarImp($param);

    }

	 public function eliminarImp(){
        $param['id'] = $this->input->post('mhdnIdImpE');
		$param['estado_imp'] = "E";

		echo $this->implementacion_model->eliminarImp($param);
    }

    function guardar() {

      $param['ruc_ter'] = $this->input->post('txt_ruc');
      $param['razon_social_ter'] = $this->input->post('txt_razon');
      $param['tipo_ter'] = "1";
      $param['representante_ter'] =$this->input->post('txt_repre');
      $param['email_ter'] = $this->input->post('txt_email');
      $param['estado_ter'] = "H";

      // $paramimpl['id'] = "6";
      $paramimpl['codigo'] = $this->input->post('txt_codigo');
      $paramimpl['descripcion'] = $this->input->post('txt_descri');
      // $paramimpl['idTercero'] = "2";
      $paramimpl['fecha_ini'] = $this->input->post('txt_fecha_ini');
      $paramimpl['fecha_ter'] = $this->input->post('txt_fecha_ter');
      $paramimpl['idUsu'] = $this->session->userdata('idUsu');
	  $paramimpl['estado_imp'] = "H";
       $lastId = $this->terceros_model->insertar($param);

      if ($lastId > 0) {
        $paramimpl['idTer']= $lastId;
        $idImple = $this->implementacion_model->guardarImpl($paramimpl);

		  		$data = array('idImple' => $idImple);
		  	echo $this->implementacion_model->result($data);
          redirect('implementacion/inicio');

      }

    }
    public function index2() {

        $data['titulo'] = 'Implementacion de Proyecto';
        $this->load->view('implementacion/documentacion');
    }

}
