<?php

if (!defined('BASEPATH'))
    exit('No esta permitido el acceso');

class Detalle extends CI_Controller {

  function __construct() {
      parent::__construct();
      $this->load->helper('form');
      $this->load->helper('url');
      $this->load->model('implementacion/implementacion_model');
	  $this->load->model('responsable/responsable_model');
  }

public function index(){
    $data = $this->input->post('txt_id');

    $this->load->view('implementacion/detalleImple',compact("data"));
}
    

    public function getDetalle(){
        $id = $this->input->post('id');
		$resultado = $this->implementacion_model->detalle($id);
		echo json_encode($resultado);
        
    }
	
	public function updateTaskResponsable(){
      $param['actividad_id'] = $this->input->post('mhdnIdA');
	  $param['responsable_id'] = $this->input->post('mtxtidResponsable');
        
	echo $this->responsable_model->updateTaskResponsable($param);
    }
	

	
	public function  getProcedure(){
		
		$resultado = $this->implementacion_model->result();
		
		echo json_encode($resultado);
		
	}
    

}
