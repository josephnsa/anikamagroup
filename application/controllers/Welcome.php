<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('welcome_model');
    }

    public function index() {
        $data['main_content'] = 'dashboard/cuerpo';
//        $clientes = $this->welcome_model->nuevos_clientes();
//        $data['clientes']=$clientes;   
//        $total_compras = $this->welcome_model->total_compras();
//        $data['total_compras']=$total_compras;  
//        $total_ventas= $this->welcome_model->total_ventas();
//        $data['total_ventas']=$total_ventas;  
//        $productos= $this->welcome_model->total_productos();
//        $data['productos']=$productos;  
//        $data['porcentaje_productos_ventas'] = $this->welcome_model->porcentaje_productos_ventas(date("m")); 
//        $data['top_cliente'] = $this->welcome_model->top_clientes_ventas(date("m"));  
//        $data['cumpleaños_clientes'] = $this->welcome_model->cumpleaños_clientes(date('Y-m-j'));
        $this->load->view('dashboard/template', $data);
    }
     public function reporte_chart_canvas() {
       $query = $this->welcome_model->reporte_chart_canvas();
       $data = array();
        foreach ($query as $key => $value) {
            $data[] = array('Mes' => $value->Mes, 'Total' => $value->Total);
        }
        echo json_encode($data);
    }
}
